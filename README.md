# 22-0467 Schrittmotortreiber

<img src="Doku/picture/Leiterplatte.PNG" style="zoom:60%;" />

This circuit board can control a stepper motor in two operating modes

- continuous mode: 
    controls the motor at a continuous speed with an adjustable acceleration
- position mode:
    controls the motor on position with the help of a measuring system (closed loop) or on the motor steps (open loop).

Dimensions:
-   wide: 50,146mm
-   height:36,169mm
-   thickness:14,9mm

## UNICAN
The CAN signals (CAN_L, CAN_H, 0V) are galvanically isolated from the supply.

### Connector X8 - UNICAN (JST-PH-08 )
 
Pin | Signal | Description 
--- | ------ | ---------- 
1 | GND | Reference potential 
2 | N.C. | Not connected 
3 | CAN_L |  
4 | 0V |  
5 | CAN_H |  
6 | 24 V |  Supply voltage logic
7 | GND | Reference potential 
8 | Motor (5…60V) |  Supply voltage Motor

## Motor
This PCB is designed for a 4-lead stepper motor and can supply a maximum phase current of 1.4A. The voltage of the motor can be fed in via the X8 pin 8 in the range of 5-60V.

### Connector X1 - MOTOR (JST-XH-04)
 
Pin | Signal | Description 
--- | ------ | ---------- 
1 | OUTA1M2 |  
2 | OUTA2M2 |  
3 | OUTB2M2 |  
4 | OUTB2M2 |  

## measuring system
This PCB is designed for a BISS-C measuring system with differential data lines and 5V power supply.

for example:
- LA11-DC-B-2D0-K-A-20D-F-0   
https://resources.renishaw.com/download.aspx?lang=en&data=103308&btn=1
- AKS16‐O2P1.28C1  
https://www.bogen-electronic.com/deu/produkte/magnetische-messk%C3%B6pfe/absoluter-magnetischer-messkopf-aks16  
(Caution: the measuring tape must be positioned very precisely)

### Connector X5 - LMS (JST-PH-07)
 
Pin | Signal | Description 
--- | ------ | ---------- 
1 | GND | Reference potential 
2 | N.C. | Not connected 
3 | +5V | Supply voltage 
4 | CLOCK+ |  
5 | CLOCK- |  
6 | DATA+ |  
7 | DATA- |  

## IOs
these connectors can be used as inputs or outputs. A current of 200mA (24V) can be driven as an output.
### Connector X2, X3, X6, X7 - AVD (JST-PH-03)
 
Pin | Signal | Description 
--- | ------ | ---------- 
1 | GND | Reference potential 
2 | 24V | logic
3 | signal | input or output