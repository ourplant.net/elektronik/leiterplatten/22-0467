/*----------------------------------------------------------------------------
 * Name:    system.h
 * Purpose: low level definitions
 * Note(s):
----------------------------------------------------------------------------- */ 
//#include <stdint.h>

#ifndef __SYSTEM_H
#define __SYSTEM_H


#if defined(STM32F767xx)
  #include "stm32f7xx.h"
#elif defined(STM32F40_41xxx)|| defined(STM32F415xx)
  #include <stm32f4xx.h>
#endif





//**** Constant ****
#define NIL ((void *)0)
#define false   0
#define true   !false

#define UI1 ((uint16_t) 1)
#define UL1 ((uint32_t) 1)
#define LENGTH(x)  (sizeof(x) / sizeof((x)[0]))
#define BITMASK(x) (UL1 << (x))
#define BOOLEAN_RESULT(x) ((x) != 0)

#define MIN(a,b) (((a)<(b))?(a):(b))
#define MAX(a,b) (((a)>(b))?(a):(b))
#define INTERVALL(a,b,c) MIN(MAX((a),(b)),(c))


//#define Unique_ID1 *((uint32_t*)0x1FFF7A10)
//#define Unique_ID2 *((uint32_t*)0x1FFF7A14)
//#define Unique_ID3 *((uint32_t*)0x1FFF7A18)
	
typedef struct 
{
  uint32_t EulerPuffer;
} tEuler;


#ifdef __cplusplus
 extern "C" {
#endif /* __cplusplus */

uint32_t Intervall(uint32_t min, uint32_t Value, uint32_t max);
uint16_t EulerFilter(uint16_t Input, uint8_t Grad, tEuler *Buffer);
unsigned int calcCRC16r(unsigned int crc, unsigned int c, unsigned int mask);
unsigned int calcCRC8(unsigned char crc, unsigned char c, unsigned char mask);
void delay_us(uint32_t time_us);

#ifdef __cplusplus
  }
#endif /* __cplusplus */
#endif

