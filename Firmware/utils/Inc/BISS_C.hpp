/*
 * BISS_C.hpp
 *
 *  Created on: 29.10.2019
 *      Author: j.selk
 */

#ifndef __BISS_C
#define __BISS_C

#include <stm32f4xx.h>

#define  MAX_HANDLERS 10
void HAL_SPI_RxCpltCallback(SPI_HandleTypeDef *hspi);


class tBiss {
private:
	uint8_t fpositionLenth;
	uint8_t CalculateDataLenth(uint64_t Data);
	uint8_t FindStart(uint64_t *rawData);
public:
	SPI_HandleTypeDef 	  *fSPI;
	tBiss(SPI_HandleTypeDef *SPI);
	~tBiss(void);
	static tBiss* handlers[MAX_HANDLERS];
	static uint16_t fInstanceCounter;
	uint16_t fMyInstanceIndex;

	uint8_t ComputePosition(void);
	uint8_t getPosLenth(void);
	void ReadPosition(void);
	int32_t Position;
	uint32_t IncPerStep;		// *1000 um nicht als float Speichern zu m�ssen
	uint8_t	andersrum;			// = 1 Messsysetem zu Motorstepps umdrehen
};

#ifdef __cplusplus
 extern "C" {
#endif /* __cplusplus */



#ifdef __cplusplus
 }
#endif /* __cplusplus */


#endif
