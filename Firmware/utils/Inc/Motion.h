/*
 * Motion.h
 *
 *  Created on: Dec 9, 2019
 *      Author: j.selk
 */

#ifdef __cplusplus
extern "C" {
#endif

#ifndef SRC_STEPPER_TEST_H_
#define SRC_STEPPER_TEST_H_
#endif /* SRC_STEPPER_TEST_H_ */
/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"
/* step clock ----------------------------------------------------------------*/
//typedef uint8_t  bool;
/// Flag interrupt priority
#define BSP_MOTOR_CONTROL_BOARD_PRIORITY_EN_AND_FAULT            (1)
/// Timer used for step clock
#define BSP_MOTOR_CONTROL_BOARD_TIM_STCK                         (TIM3)

/// Timer output for step clock
#define BSP_MOTOR_CONTROL_BOARD_OUTPUT_TIM_STCK                  (TIMER_MAIN_OUTPUT)

/// Channel Timer used for step clock
#define BSP_MOTOR_CONTROL_BOARD_CHAN_TIM_STCK                    (TIM_CHANNEL_2)

/// HAL Active Channel Timer used for step clock
#define BSP_MOTOR_CONTROL_BOARD_HAL_ACT_CHAN_TIM_STCK            (HAL_TIM_ACTIVE_CHANNEL_2)

/// Timer Clock Enable for step clock
#define __BSP_MOTOR_CONTROL_BOARD_CLCK_ENABLE_TIM_STCK()         __TIM3_CLK_ENABLE()

/// Timer Clock Disable for step clock
#define __BSP_MOTOR_CONTROL_BOARD_CLCK_DISABLE_TIM_STCK()        __TIM3_CLK_DISABLE()

/// Step clock global interrupt
#define BSP_MOTOR_CONTROL_BOARD_IRQn_TIM_STCK                    (TIM3_IRQn)

/// Step clock global interrupt priority
#define BSP_MOTOR_CONTROL_BOARD_PRIORITY_TIM_STCK                (BSP_MOTOR_CONTROL_BOARD_PRIORITY_EN_AND_FAULT - 1)

/// step clock GPIO alternate function
#define BSP_MOTOR_CONTROL_BOARD_AF_TIM_STCK                      (GPIO_AF2_TIM3)

/// Maximum frequency of the step clock frequency in Hz
#define Motion_MAX_STCK_FREQ          (60000)					//60000= 30kHz

/// Minimum frequency of the step clock frequency in Hz
#define Motion_MIN_STCK_FREQ          (0)

/// GPIO Pin used for the Motion step clock pin
#define BSP_MOTOR_CONTROL_BOARD_PIN_TIM_STCK_MODE3            (GPIO_PIN_5)
/// GPIO Port used for the Motion step clock pin
#define BSP_MOTOR_CONTROL_BOARD_PORT_TIM_STCK_MODE3           (GPIOB)

/// GPIO Pin used for the STSPIN220 direction pin
#define BSP_MOTOR_CONTROL_BOARD_PIN_DIR_MODE4                 (GPIO_PIN_11)
/// GPIO port used for the STSPIN220 direction pin
#define BSP_MOTOR_CONTROL_BOARD_PORT_DIR_MODE4                (GPIOA)

/// direction change bit mask
#define Motion_DIR_CHANGE_BIT_MASK    (0x08)

/// Max position
#define Motion_MAX_POSITION           (0x7FFFFFFF)

/// Min position
#define Motion_MIN_POSITION           (0x80000000)

/// Position range
#define Motion_POSITION_RANGE         ((uint32_t)(Motion_MAX_POSITION -\
                                                        Motion_MIN_POSITION))

typedef enum {
	ACCELERATING = 0,
	DECELERATINGTOSTOP = 1,
	DECELERATING = 2,
	STEADY = 3,
	INDEX_ACCEL = 4,
	INDEX_RUN = 5,
	INDEX_DECEL = 6,
	INDEX_DWELL = 7,
	INACTIVE = 8,
	STANDBY = 9,
	STANDBYTOINACTIVE = 10
} motorState_t;

typedef enum {
	BACKWARD = 0, FORWARD = 1, UNKNOW_DIR = ((uint8_t) 0xFF)
} motorDir_t;
typedef enum {
	NO_CMD = 0x00,
	RUN_CMD = 0x01,
	MOVE_CMD = 0x02,
	SoftSTOP = 0x04,
	DirChange = 0x08
} deviceCommand_t;
void Motion_Init(uint8_t *LMS, uint8_t *andersrum, uint32_t *IncPerStepp);
//uint8_t Motion_Board_SetModePins();
//Start step clock
void Motion_Board_TimStckStart(void);
//Set step clock frequency and start it
void Motion_Board_TimStckSetFreq(uint16_t newFreq);
//Step clock compare value initialization
void Motion_Board_TimStckCompareInit(void);
//Init the timer
void Motion_Board_TimStckInit(void);
//DeInit the timer
void Motion_Board_TimStckDeInit(void);
//Stop the timer
uint8_t Motion_Board_TimStckStop(volatile uint8_t *pToggleOdd);
int32_t Motion_GetPosition(void);         //Return the ABS_POSITION (32b signed)
int8_t Motion_GetInPosition(void);
int32_t Motion_GetPositionError(void);
void Motion_ResetInPosition(void);
int16_t Motion_GetSpeed(void);
int16_t Motion_GetAcceleration(void);
void BSP_MotorControl_StepClockHandler();
void Motion_ApplySpeed(uint16_t newSpeed);
void Motion_StepClockHandler();
//Move the motor of the specified number of steps
void Motion_Move(motorDir_t direction, uint32_t stepCount);
void Motion_StartMovement();
void Motion_ComputeSpeedProfile(uint32_t nbSteps);
void Motion_SetMaxSpeed(uint16_t newMaxSpeed);
void Motion_SetMinSpeed(uint16_t newMinSpeed);
void Motion_SetAcceleration(uint16_t newAcc);
void Motion_SetDirection(motorDir_t direction);   //Set the Motion direction pin
void Motion_Run(motorDir_t direction);       //Run the motor
void Motion_SoftStop(); //Progressively stop the motor by using the device deceleration and set deceleration torque
//Set the state of the direction GPIO
void Motion_Board_SetDirectionGpio(uint8_t gpioState);
motorState_t Motion_GetDeviceState(void);         //Return the device state
uint8_t Motion_GetAktive(void);
void Motion_GoTo(int32_t targetPosition, int32_t istPosition);   //Go to the specified position

/// Device Parameters Structure Type
typedef struct {
	/// accumulator used to store speed increase smaller than 1 pps
	volatile uint32_t accu;
	/// Position in microstep according to current step mode
	volatile int32_t currentPosition;
	/// position in microstep at the end of the accelerating phase
	volatile uint32_t endAccPos;
	/// nb of in microstep performed from the beggining of the goto or the move command
	/// command under execution
	volatile deviceCommand_t commandExecuted;
	volatile uint32_t relativePos;
	/// position in microstep step at the start of the decelerating phase
	volatile uint32_t startDecPos;
	/// nb of microstep steps to perform for the goto or move commands
	uint32_t stepsToTake;
	/// acceleration in pps^2
	volatile uint16_t acceleration;
	/// deceleration in pps^2
	volatile uint16_t deceleration;
	/// max speed in pps (speed use for goto or move command)
	volatile uint16_t maxSpeed;
	/// min speed in pps
	volatile uint16_t minSpeed;
	/// current speed in pps
	volatile uint16_t speed;
	/// FORWARD or BACKWARD direction
	volatile motorDir_t direction;
	/// current state of the device
	volatile motorState_t motionState;
	/// Längenmessystem aktiv
	volatile uint8_t *LMS;
	volatile uint32_t *IncPerStep;
	volatile uint8_t *andersrum;
	volatile uint8_t inPos;
} deviceParams_t;

/// Motor driver structure definition
typedef struct {
	/// Function pointer to StepClockHandler
	void (*StepClockHandler)();
} motorDrv_t;

#ifdef __cplusplus
}
#endif

