/*----------------------------------------------------------------------------
 * Name:    KameraMUX.h
 * Purpose: low level definitions
 * Note(s):
 ----------------------------------------------------------------------------- */

#ifndef __STEPPER_H
#define __STEPPER_H

#include <stm32f4xx.h>

#if (defined __GNUC__)
#  define swap64(u64) ((uint64_t)__builtin_bswap64((uint64_t)(u64)))
#  define swap32(u32) ((uint32_t)__builtin_bswap32((uint32_t)(u32)))
#  define swap16(u16) ((uint16_t)__builtin_bswap16((uint16_t)(u16)))
#else
#define swap64(u64) ((uint64_t)(((uint64_t)swap32((uint64_t)(u64) >> 32)) |\
                      ((uint64_t)swap32((uint64_t)(u64)) << 32)))
#define swap32(u32) ((uint32_t)(((uint32_t)Swap16((uint32_t)(u32) >> 16)) |\
                     ((uint32_t)Swap16((uint32_t)(u32)) << 16)))
#define swap16(u16) ((uint16_t)(((uint16_t)(u16) >> 8) |\
                                ((uint16_t)(u16) << 8)))
#endif

/***************************************/
/* CHOPCONF  Chopper Configuration     */
/***************************************/
#define CHOPCONF   (unsigned long) 0x080000  
/* BIT POSITIONS */
#define TBL_POS    (uint8_t) 15   // 2 bit blank time select
#define CHM_POS    (uint8_t) 14   // chopper mode
#define RNDTF_POS  (uint8_t) 13   // random TOFF time
#define HDEC_POS   (uint8_t) 11   // 2 Bit hysteresis decrement interval or fast decay mode 
#define HEND_POS   (uint8_t) 7    // 4 bit hysteresis low value or sine wave offset 
#define HSTRT_POS  (uint8_t) 4    // 3 Bit hysteresis start value or fast decay time setting 
#define TOFF_POS   (uint8_t) 0    // 4 Bit off time and driver enable
/******************************************/
/* SMARTEN Smart Energy control coolStep  */
/******************************************/
#define SMARTEN   (unsigned long) 0x0A0000
/* BIT POSITIONS */
#define SEIMIN_POS (uint8_t) 15   // minimum current for smart current control 
#define SEDN_POS   (uint8_t) 13   // 2 bit current down step speed 
#define SEMAX_POS  (uint8_t) 8    // 4 Bit stallGuard hysteresis value for smart current control
#define SEUP_POS   (uint8_t) 5    // 2 bit current up step width
#define SEMIN_POS  (uint8_t) 0    // 4 Bit minimum stallGuard value for smart current control and smart current enable 
/*******************************************************************/
/* SGCSCONF Load measurement stallGuard2� and Current Setting      */
/*******************************************************************/
#define SGCSCONF  (unsigned long) 0x0C0000
/* BIT POSITIONS */
#define SFILT_POS (uint8_t) 16   // stallGuard filter enable
#define SGT_POS   (uint8_t) 8   // 7 stallGuard threshold value 
#define CS_POS    (uint8_t) 0   // 5 current scale (scales digital currents A and B) 
/************************************/
/* DRVCONF Driver Configuration     */
/************************************/
#define DRVCONF   (unsigned long) 0x0E0000  
/* BIT POSITIONS */
#define TST_POS    (uint8_t) 16   // reserved TEST mode
#define SLPH_POS   (uint8_t) 14   // 2 Bit Slope control high side
#define SLPL_POS   (uint8_t) 12   // 2 Bit Slope control low side
#define DISS2G_POS (uint8_t) 10   // short to GND protection disable
#define TS2G_POS   (uint8_t) 8    // 2 Bit short to GND detection timer
#define SDOFF_POS  (uint8_t) 7    // Step Direction input off 
#define VSENSE_POS (uint8_t) 6    // sense resistor voltage based current scaling (Full scale sense resistor voltage is 0:305mV 1:165mV) 
#define RDSEL_POS  (uint8_t) 4    // 2 Bit Select value for read out (RD bits)
/*****************************************************/
/* DRVCTRL Driver control register bit assignment    */
/*****************************************************/
#define DRVCTRL   (uint8_t) 0x000000
/* SDOFF=0 StepDir mode */
#define INTPOL_POS  (uint8_t) 9   // enable step interpolation 
#define DEDGE_POS   (uint8_t) 8   // enable double edge step pulses
#define MRES_POS    (uint8_t) 0   // 4 bit micro step resolution 0-8 = 256, 128, 64, 32, 16, 8, 4, 2, FULLSTEP
/* SDOFF=1 SPI mode */
#define PHA_POS     (uint8_t) 17   // Polarity A
#define CA_POS      (uint8_t) 9    // 8 bit Current  A
#define PHB_POS     (uint8_t) 8    // Polarity B
#define CB_POS      (uint8_t) 0    // 8 bitCurrent  B
/*****************************************************/
/*****************************************************/
/* DRVSTATUS read status information � Partially selected by RDSEL in DRVCONF    */
/*****************************************************/
#define STATUSDATA_POS  (uint8_t) 10  
#define STST_POS   (uint8_t) 7   // stand still step indicator 
#define OLB_POS    (uint8_t) 6   // open load indicator B
#define OLA_POS    (uint8_t) 5   // open load indicator A
#define S2GB_POS   (uint8_t) 4   // short to GND detection 
#define S2GA_POS   (uint8_t) 3   // short to GND detection  A
#define OTPW_POS   (uint8_t) 2   // Polarity B
#define OT_POS     (uint8_t) 1   // 8 bitCurrent  B
#define SG_POS     (uint8_t) 0   // stallGuard status 
/*****************************************************/

#define MicroStepResolution_FULLSTEP  (uint32_t) 8
#define MicroStepResolution_2    			(uint32_t) 7
#define MicroStepResolution_4    			(uint32_t) 6
#define MicroStepResolution_8    			(uint32_t) 5
#define MicroStepResolution_16   			(uint32_t) 4
#define MicroStepResolution_32  		    (uint32_t) 3
#define MicroStepResolution_64  		    (uint32_t) 2
#define MicroStepResolution_128  		    (uint32_t) 1
#define MicroStepResolution_256  		    (uint32_t) 0


const float StallGuardAnstieg =-0.00067901;
const float StallGuardOffset = 6.93827160;

class tStepper {

private:
	uint8_t fInit;
	uint8_t fEnabled;
	uint8_t fStepInProzess;
	uint32_t fStepsPerTurn;

	uint32_t fDRVCTRL;
	uint32_t fCHOPCONF;
	uint32_t fSMARTEN;
	uint32_t fSGCSCONF;
	uint32_t fDRVCONF;

	SPI_HandleTypeDef *fSPI;

	GPIO_TypeDef*   fCSPort;
	uint16_t        fCS_Pin;
	GPIO_TypeDef*   fDisablePort;
	uint16_t        fDisable_Pin;

	uint32_t tmc_write(uint32_t value);
	uint32_t cs;

	#define StallGuardFilterEnable		 (uint32_t) 0 << SFILT_POS

public:
	tStepper(SPI_HandleTypeDef *SPI,GPIO_TypeDef* CSPort, uint16_t CS_Pin, GPIO_TypeDef* DisablePort, uint16_t DisablePin, uint32_t StepsPerTurn);

	void ResetData(void);
	void UpdateTmcStatus(void);
	void tmc_init(double current);
	void tmc_denit(void);
	void SetSGCSconf(uint16_t speed);
	void tmc_DRVCTRL(uint32_t value);
	void tmc_CHOPCONF(uint32_t value);
	void tmc_SMARTEN(uint32_t value);
	void tmc_SGCSCONF(uint32_t value);
	void tmc_DRVCONF(uint32_t value);
	void tmc_RDSEL(uint8_t);

	void StallGuardThreshold(int8_t value);

	void Enable(void);
	void Disable(void);
	uint8_t OpenLoad(void);
	uint8_t ShortToGND(void);
	uint8_t TemperatureWarning(void);
	uint8_t TemperatureError(void);
	uint8_t StallGuard(void);

	uint8_t Enabled(void);
	uint8_t InPosition(void);

	uint32_t TmcStatus;
	uint8_t Fehler;
	uint16_t StallGuardThresholdValue;
};

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */
// tStepper class;

void SPI_INIT(SPI_TypeDef *SPI, GPIO_TypeDef* SPIIOx, uint32_t SPI_PinBits);

#ifdef __cplusplus
}
#endif /* __cplusplus */	 

#endif 
