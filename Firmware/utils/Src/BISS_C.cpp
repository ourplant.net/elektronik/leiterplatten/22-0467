#include "BISS_C.h"
#define BITMASK(x) ((uint64_t)1 << (x))

extern uint8_t dataRx[8];
extern void writeEEPROM(uint8_t Register, uint32_t data);
uint8_t tableCRC6[64] = { 0x00, 0x03, 0x06, 0x05, 0x0C, 0x0F, 0x0A, 0x09, 0x18, 0x1B, 0x1E, 0x1D, 0x14, 0x17, 0x12, 0x11, 0x30, 0x33, 0x36, 0x35, 0x3C, 0x3F, 0x3A, 0x39, 0x28, 0x2B, 0x2E, 0x2D, 0x24, 0x27, 0x22, 0x21, 0x23, 0x20, 0x25, 0x26, 0x2F, 0x2C, 0x29, 0x2A, 0x3B, 0x38, 0x3D, 0x3E, 0x37, 0x34, 0x31, 0x32, 0x13, 0x10, 0x15, 0x16, 0x1F, 0x1C, 0x19, 0x1A, 0x0B, 0x08, 0x0D, 0x0E, 0x07, 0x04, 0x01, 0x02 };
tBiss* tBiss::handlers[MAX_HANDLERS] = {0};
uint16_t tBiss::fInstanceCounter = 0;

tBiss::tBiss(SPI_HandleTypeDef *SPI) {
	fSPI = SPI;
	fMyInstanceIndex = fInstanceCounter;
	handlers[fInstanceCounter++] = this; // register this instance against the
	fpositionLenth = 0;
	}

tBiss::~tBiss(void) {
	fInstanceCounter--;
	 for (int i=fMyInstanceIndex; i<fInstanceCounter; i++)
	    handlers[i] = handlers[i+1];
}

uint8_t crcBiSS(uint32_t inputData) {
	uint8_t crc;
	uint32_t t;
	t = ((inputData >> 30) & 0x00000003);
	crc = ((inputData >> 24) & 0x0000003F);
	t = crc ^ tableCRC6[t];
	crc = ((inputData >> 18) & 0x0000003F);
	t = crc ^ tableCRC6[t];
	crc = ((inputData >> 12) & 0x0000003F);
	t = crc ^ tableCRC6[t];
	crc = ((inputData >> 6) & 0x0000003F);
	t = crc ^ tableCRC6[t];
	crc = (inputData & 0x0000003F);
	t = crc ^ tableCRC6[t];
	crc = tableCRC6[t];
	return crc;
}

void tBiss::ReadPosition(void){
  if (HAL_SPI_GetState(fSPI) == HAL_SPI_STATE_READY)
	HAL_SPI_Receive_DMA(fSPI, dataRx, 8);
}

void HAL_SPI_RxCpltCallback(SPI_HandleTypeDef *hspi) {
	for (int i=0; i<tBiss::fInstanceCounter; i++)
	  {
		if (tBiss::handlers[i]->fSPI == hspi)
		 {
			tBiss::handlers[i]->ComputePosition();
		 }
	  }
}

uint8_t tBiss::FindStart(uint64_t *rawData){
	for (int i=0; i<8;i++)
	  *(((uint8_t *) rawData) + 7-i) = dataRx[i];

	uint8_t timeout =0 ;
	while((*rawData & BITMASK(63)) > 0){			//ACK finden
		*rawData = *rawData <<1;
		timeout ++;
		if (timeout > 64) return 4;
	}

	timeout =0 ;
	while((*rawData & BITMASK(63)) ==0){			//Start finden
		*rawData = *rawData <<1;
		timeout ++;
		if (timeout > 64) return 4;
	}

	*rawData = *rawData <<2;						//Start und das nachste Bit ist 0, muss auch ignoriert werden
	return 0;
}
uint8_t tBiss::CalculateDataLenth(uint64_t Data){
	uint64_t tempData;
	uint8_t var;

	for (var = 20; var < 55; ++var) {
		tempData = Data >> (64-(var+8));
		uint8_t readCRC = tempData & 0b111111;
		tempData = tempData >> 6;					//CRRC entfernen

		uint8_t calcCRC = crcBiSS(tempData);		//Positions Bits + Warning + Fehler
		if ((~readCRC & 63) == calcCRC)				//readCRC Invertieren und mit calcCRC vergleichen
			return var;
	}
	return 0;
}

uint8_t tBiss::getPosLenth(void) {
	return fpositionLenth;
}

void tBiss::setPosLenth(uint8_t lenth) {
	fpositionLenth = lenth;
}

uint8_t tBiss::ComputePosition(void) {
//	fpositionLenth = 24;
	uint64_t rawData = 0 ;

	if(FindStart(&rawData) == 0){
		if(fpositionLenth == 0 or fpositionLenth == 0xff){
			fpositionLenth = CalculateDataLenth(rawData);
			writeEEPROM(3, fpositionLenth);
		}

		rawData = rawData >> (64-(fpositionLenth+8));//auf richtige L�nge zusammenstutzen

		uint8_t readCRC = rawData & 0b111111;
		rawData = rawData >> 6;						//CRRC entfernen

		if ((rawData & 0b00000001) == 0)			//Warning Bit 0 = Fehler
			return 1;
		if ((rawData & 0b00000010) == 0)			//Error Bit 0 = Fehler
			return 2;

		uint8_t calcCRC = crcBiSS(rawData);			//Positions Bits + Warning + Fehler
		if ((~readCRC & 63) != calcCRC)				//readCRC Invertieren und mit calcCRC vergleichen
			return 3;

		else
			Position = rawData>>2;					//ohne Warning + Fehler

		return 0;
	}else
		return 4;
 }

