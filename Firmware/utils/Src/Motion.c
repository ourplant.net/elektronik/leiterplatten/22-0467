/*
 * Motion.c
 *
 *  Created on: Dec 9, 2019
 *      Author: j.selk
 *
 *  Grundlage ist stspin220.c aus der X-CUBE-SPN6 Library
 *
 */
#include "Motion.h"
#include "stm32f4xx_hal.h"
#include <math.h>

/// Timer Prescaler for step clocks
#define TIMER_PRESCALER (64)

/// Timer handler for step clock
TIM_HandleTypeDef hTimerStepClock;
/// Step clock compare value
volatile uint32_t ccrValue;
static volatile uint8_t toggleOdd;

/// Motion Device Paramaters structure
deviceParams_t devicePrm;
static motorDrv_t *motorDrvHandle = 0;
extern int32_t fIstPosition;
extern int32_t fZielposition;

void Motion_Init(uint8_t *LMS, uint8_t *andersrum, uint32_t *IncPerStepp) {
	devicePrm.motionState = INACTIVE;
	devicePrm.LMS = LMS;
	devicePrm.maxSpeed = 1000;
	devicePrm.acceleration = 100;
	devicePrm.deceleration = 100;
	devicePrm.andersrum = andersrum;
	devicePrm.IncPerStep = IncPerStepp;
}

/******************************************************//**
 * @brief  Starts the step clock
 * @retval None
 * @note The frequency is directly the current speed of the device
 **********************************************************/
void Motion_Board_TimStckStart() {
	/* Clear pending timer interrupt */
	if (BSP_MOTOR_CONTROL_BOARD_CHAN_TIM_STCK == TIM_CHANNEL_1) {
		__HAL_TIM_CLEAR_IT(&hTimerStepClock, TIM_IT_CC1);
	} else if (BSP_MOTOR_CONTROL_BOARD_CHAN_TIM_STCK == TIM_CHANNEL_2) {
		__HAL_TIM_CLEAR_IT(&hTimerStepClock, TIM_IT_CC2);
	} else if (BSP_MOTOR_CONTROL_BOARD_CHAN_TIM_STCK == TIM_CHANNEL_3) {
		__HAL_TIM_CLEAR_IT(&hTimerStepClock, TIM_IT_CC3);
	} else if (BSP_MOTOR_CONTROL_BOARD_CHAN_TIM_STCK == TIM_CHANNEL_4) {
		__HAL_TIM_CLEAR_IT(&hTimerStepClock, TIM_IT_CC4);
	}
	/* Start timer interrupts */
#if (BSP_MOTOR_CONTROL_BOARD_OUTPUT_TIM_STCK == TIMER_MAIN_OUTPUT)
	if ((hTimerStepClock.Instance->CCER
			&\
 (TIM_OCPOLARITY_HIGH << BSP_MOTOR_CONTROL_BOARD_CHAN_TIM_STCK))
			== 0) {
		hTimerStepClock.Instance->CCER |= (TIM_OCPOLARITY_HIGH
				<< BSP_MOTOR_CONTROL_BOARD_CHAN_TIM_STCK);
	} else {
		hTimerStepClock.Instance->CCER &= ~(TIM_OCPOLARITY_HIGH
				<< BSP_MOTOR_CONTROL_BOARD_CHAN_TIM_STCK);
	}
	HAL_TIM_OC_Start_IT(&hTimerStepClock,
	BSP_MOTOR_CONTROL_BOARD_CHAN_TIM_STCK);
#endif /* (BSP_MOTOR_CONTROL_BOARD_OUTPUT_TIM_STCK == TIMER_MAIN_OUTPUT) */
}

/// Motion motor driver functions pointer structure
motorDrv_t MotionDrv = { Motion_StepClockHandler //void (*StepClockHandler)(uint8_t deviceId);
		};

motorDrv_t* Motion_GetMotorHandle(void) {
	return (&MotionDrv);
}

/******************************************************//**
 * @brief Step clock compare value initialization
 * @retval None
 **********************************************************/
void Motion_Board_TimStckCompareInit(void) {
	ccrValue = hTimerStepClock.Instance->CNT;
	motorDrvHandle = Motion_GetMotorHandle();

}

/******************************************************//**
 * @brief  Sets the frequency of step clock
 * @param[in] newFreq in Hz
 * @retval None
 * @note The frequency is directly the current speed of the device
 **********************************************************/
void Motion_Board_TimStckSetFreq(uint16_t newFreq) {
	ccrValue += (HAL_RCC_GetSysClockFreq()
			/\
 (TIMER_PRESCALER * 2 * (uint32_t) newFreq));
	__HAL_TIM_SetCompare(&hTimerStepClock,
			BSP_MOTOR_CONTROL_BOARD_CHAN_TIM_STCK,
			ccrValue);
}

/******************************************************//**
 * @brief  Initialises the timer used for the step clock
 * @retval None
 **********************************************************/
void Motion_Board_TimStckInit(void) {
	TIM_OC_InitTypeDef sConfigOC = { 0 };
	TIM_MasterConfigTypeDef sMasterConfig = { 0 };
	TIM_HandleTypeDef *pHTim;

	pHTim = &hTimerStepClock;
	pHTim->Instance = BSP_MOTOR_CONTROL_BOARD_TIM_STCK;
	pHTim->Init.CounterMode = TIM_COUNTERMODE_UP;
	pHTim->Init.Prescaler = TIMER_PRESCALER - 1;
	if ((pHTim->Instance != TIM2) && (pHTim->Instance != TIM5)) {
		pHTim->Init.Period = 0xFFFF;
	} else {
		pHTim->Init.Period = 0xFFFFFFFF;
	}
	pHTim->Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
	pHTim->Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
	HAL_TIM_OC_Init(pHTim);

	sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
	sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
	sConfigOC.Pulse = 0;
	if (pHTim->Instance != TIM4) {
		/* Setting the OCMode to TIM_OCMODE_FORCED_ACTIVE ensures that on the */
		/* first interrupt occuring in the toggle mode, a rising edge will occur */
		sConfigOC.OCMode = TIM_OCMODE_FORCED_ACTIVE;
		HAL_TIM_OC_ConfigChannel(pHTim, &sConfigOC,
		BSP_MOTOR_CONTROL_BOARD_CHAN_TIM_STCK);
		sConfigOC.OCMode = TIM_OCMODE_TOGGLE;
	} else {
		sConfigOC.OCMode = TIM_OCMODE_TIMING;

	}
	HAL_TIM_OC_ConfigChannel(pHTim, &sConfigOC,
	BSP_MOTOR_CONTROL_BOARD_CHAN_TIM_STCK);

	sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
	sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
	HAL_TIMEx_MasterConfigSynchronization(pHTim, &sMasterConfig);
}

/******************************************************//**
 * @brief  DeInitialises the timer used for the step clock
 * @retval None
 **********************************************************/
void Motion_Board_TimStckDeInit(void) {
	HAL_TIM_OC_DeInit(&hTimerStepClock);
}

/******************************************************//**
 * @brief  Stops the timer
 * @param[in] pToggleOdd pointer to the volatile toggleOdd variable
 * @retval 1 if OK, 0 if STCK MODE3 pin is high (forbidden configuration)
 **********************************************************/
uint8_t Motion_Board_TimStckStop(volatile uint8_t *pToggleOdd) {
	devicePrm.motionState = INACTIVE;
	devicePrm.speed = 0;
//	__disable_irq();
	if (*pToggleOdd == 1) {
//		__enable_irq();
		return 1;
	}
#if (BSP_MOTOR_CONTROL_BOARD_OUTPUT_TIM_STCK == TIMER_COMPLEMENTARY_OUTPUT)
	HAL_TIMEx_OCN_Stop_IT(&hTimerStepClock, BSP_MOTOR_CONTROL_BOARD_CHAN_TIM_STCK);
#else
	HAL_TIM_OC_Stop_IT(&hTimerStepClock, BSP_MOTOR_CONTROL_BOARD_CHAN_TIM_STCK);
#endif
//	__enable_irq();
	/* DeInitialize the step clock timer */
	Motion_Board_TimStckDeInit();
	return 1;
}

/******************************************************//**
 * @brief Request the motor to move to the specified position
 **********************************************************/
void Motion_GoTo(int32_t targetPosition, int32_t istPosition) {
	motorDir_t direction;
	devicePrm.currentPosition = istPosition;

	if (targetPosition > devicePrm.currentPosition) {
		devicePrm.stepsToTake = targetPosition - devicePrm.currentPosition;
		if (devicePrm.stepsToTake < (Motion_POSITION_RANGE >> 1)) {
			direction = FORWARD;
		} else {
			direction = BACKWARD;
			devicePrm.stepsToTake = Motion_POSITION_RANGE
					- devicePrm.stepsToTake;
		}
	} else {
		devicePrm.stepsToTake = devicePrm.currentPosition - targetPosition;
		if (devicePrm.stepsToTake < (Motion_POSITION_RANGE >> 1)) {
			direction = BACKWARD;
		} else {
			direction = FORWARD;
			devicePrm.stepsToTake = Motion_POSITION_RANGE
					- devicePrm.stepsToTake;
		}
	}

	if (*devicePrm.LMS) {
    if (*devicePrm.andersrum)
      direction = !direction;
    devicePrm.stepsToTake = devicePrm.stepsToTake / ((float) *devicePrm.IncPerStep / 1000);
  }
  // Todo: Leider gibt es an der nLight immer noch Stellen wo dsa System schwingt
	if (devicePrm.stepsToTake != 0) {
		if (devicePrm.stepsToTake > 7) {
		  devicePrm.commandExecuted = MOVE_CMD;
			/* Direction setup */
			devicePrm.inPos = 0;
			Motion_SetDirection(direction);
			Motion_ComputeSpeedProfile(devicePrm.stepsToTake);
			/* Motor activation */
			Motion_StartMovement();
		} else
			devicePrm.inPos = 1;
	}else
		devicePrm.inPos = 1;
}

/******************************************************//**
 * @brief  Stops the motor by using the device deceleration
 * @retval true if the command is successfully executed, else false
 * @note The command is not performed if the device is in INACTIVE,
 * STANDBYTOINACTIVE or STANDBY state.
 **********************************************************/
void Motion_SoftStop() {
	if ((devicePrm.motionState & INACTIVE) != INACTIVE) {
		devicePrm.commandExecuted |= SoftSTOP;
	}
}
/******************************************************//**
 * @brief  Moves the motor of the specified number of steps
 * @param[in] direction FORWARD or BACKWARD
 * @param[in] stepCount Number of steps to perform
 * @retval None
 **********************************************************/
void Motion_Move(motorDir_t direction, uint32_t stepCount) {
	if (stepCount != 0) {
		devicePrm.stepsToTake = stepCount;
		devicePrm.commandExecuted = MOVE_CMD;
		/* Direction setup */
		Motion_SetDirection(direction);
		Motion_ComputeSpeedProfile(stepCount);

		/* Motor activation */
		Motion_StartMovement(0);
	}
}
/******************************************************//**
 * @brief  Runs the motor. It will accelerate from the min
 * speed up to the max speed by using the device acceleration.
 * @param[in] direction FORWARD or BACKWARD
 * @retval None
 **********************************************************/
void Motion_Run(motorDir_t direction) {
	/* Direction setup */
	Motion_SetDirection(direction);
	/* Motor activation */
	devicePrm.commandExecuted = RUN_CMD;
	Motion_StartMovement();
}

void Motion_Board_SetDirectionGpio(uint8_t gpioState) {
	HAL_GPIO_WritePin(BSP_MOTOR_CONTROL_BOARD_PORT_DIR_MODE4,
	BSP_MOTOR_CONTROL_BOARD_PIN_DIR_MODE4, (GPIO_PinState) gpioState);
}

/******************************************************//**
 * @brief  Specifies the direction
 * @param[in] deviceId Unused parameter
 * @param[in] dir FORWARD or BACKWARD
 * @note The direction change is applied if the device
 * is in INACTIVE or STANDBY state or if the device is
 * executing a run command
 * @retval None
 **********************************************************/
void Motion_SetDirection(motorDir_t dir) {
	if ((devicePrm.motionState == INACTIVE)
			|| (devicePrm.motionState == STANDBY)) {
		devicePrm.direction = dir;
		Motion_Board_SetDirectionGpio(dir);
	} else if ((devicePrm.commandExecuted & RUN_CMD) != 0) {
		devicePrm.commandExecuted |= Motion_DIR_CHANGE_BIT_MASK;
	}

}

void Motion_SetMaxSpeed(uint16_t newMaxSpeed) {
	devicePrm.maxSpeed = newMaxSpeed;
}

void Motion_SetMinSpeed(uint16_t newMinSpeed) {
	devicePrm.minSpeed = newMinSpeed;
}
void Motion_SetAcceleration(uint16_t newAcc) {
	devicePrm.acceleration = newAcc;
	devicePrm.deceleration = newAcc;
}
/******************************************************//**
 * @brief Return the current position value of the specified device
 * @retval current position value
 **********************************************************/
int32_t Motion_GetPosition(void) {
	return devicePrm.currentPosition;
}
int32_t Motion_GetPositionError(void) {
  if (devicePrm.direction)
    return 0-devicePrm.stepsToTake;
  else
    return devicePrm.stepsToTake;
}
int8_t Motion_GetInPosition(void) {
	return devicePrm.inPos;
}
void Motion_ResetInPosition(void) {
	devicePrm.inPos = 0;
}
int16_t Motion_GetSpeed(void) {
	return devicePrm.speed;
}
int16_t Motion_GetAcceleration(void) {
	return devicePrm.acceleration;
}
/******************************************************//**
 * @brief  Computes the speed profile according to the number of steps to move
 * @param[in] deviceId Unused parameter
 * @param[in] nbSteps number of steps to perform
 * @retval None
 * @note Using the acceleration and deceleration of the device,
 * this function determines the duration in steps of the acceleration,
 * steady and deceleration phases.
 * If the total number of steps to perform is big enough, a trapezoidal move
 * is performed (i.e. there is a steady phase where the motor runs at the maximum
 * speed.
 * Else, a triangular move is performed (no steady phase: the maximum speed is never
 * reached.
 **********************************************************/
void Motion_ComputeSpeedProfile(uint32_t nbSteps) {
	uint32_t reqAccSteps;
	uint32_t reqDecSteps;

	/* compute the number of steps to get the targeted speed */
	uint16_t minSpeed = devicePrm.minSpeed;
	reqAccSteps = (devicePrm.maxSpeed - minSpeed);
	reqAccSteps *= (devicePrm.maxSpeed + minSpeed);
	reqDecSteps = reqAccSteps;
	reqAccSteps /= (uint32_t) devicePrm.acceleration;
	reqAccSteps /= 2;

	/* compute the number of steps to stop */
	reqDecSteps /= (uint32_t) devicePrm.deceleration;
	reqDecSteps /= 2;

	if ((reqAccSteps + reqDecSteps) > nbSteps) {
		/* Triangular move  */
		/* reqDecSteps = (Pos * Dec) /(Dec+Acc) */
		uint32_t dec = devicePrm.deceleration;
		uint32_t acc = devicePrm.acceleration;

		reqDecSteps = ((uint32_t) dec * nbSteps) / (acc + dec);
		if (reqDecSteps > 1) {
			reqAccSteps = reqDecSteps - 1;
			if (reqAccSteps == 0) {
				reqAccSteps = 1;
			}
		} else {
			reqAccSteps = 0;
		}

//		reqDecSteps -= (devicePrm.deceleration / 4); // ansonsten bremst die Rampe nicht auf null.

		devicePrm.endAccPos = reqAccSteps;
		devicePrm.startDecPos = reqDecSteps;
	} else {
		/* Trapezoidal move */
		/* accelerating phase to endAccPos */
		/* steady phase from  endAccPos to startDecPos */
		/* decelerating from startDecPos to stepsToTake*/
		devicePrm.endAccPos = reqAccSteps;
		devicePrm.startDecPos = nbSteps - reqDecSteps - 1;
	}
}
/******************************************************//**
 * @brief Initialises the bridge parameters to start the movement
 * and enable the power bridge
 * @param[in] RunMode 1= Continuierlich
 * @retval None
 **********************************************************/
void Motion_StartMovement() {
	/* Enable Motion powerstage */
//	Motion_Enable(deviceId);
	toggleOdd = 0;
	devicePrm.accu = 0;
	devicePrm.relativePos = 0;
	if ((devicePrm.endAccPos == 0) && (devicePrm.commandExecuted != RUN_CMD)) {
		devicePrm.motionState = DECELERATING;
	} else {
		devicePrm.motionState = ACCELERATING;
	}
	/* Initialize the step clock timer */
	Motion_Board_TimStckInit();
	/* Program the step clock */
	Motion_Board_TimStckCompareInit();
	Motion_ApplySpeed(devicePrm.minSpeed);
	Motion_Board_TimStckStart();
}

/******************************************************//**
 * @brief Return the device state
 * @param[in] deviceId Unused parameter
 * @retval State (ACCELERATING, DECELERATING, STEADY or INACTIVE)
 **********************************************************/
motorState_t Motion_GetDeviceState(void) {
	return devicePrm.motionState;
}

uint8_t Motion_GetAktive(void) {
	if (Motion_GetDeviceState() == INACTIVE) {
		return 0;
	} else {
		return 1;
	}
}
/******************************************************//**
 * @brief  Handles the device state machine at each step
 * @retval None
 * @note Must only be called by the timer ISR
 **********************************************************/
void BSP_MotorControl_StepClockHandler() {
	motorDrvHandle->StepClockHandler();
}

/******************************************************//**
 * @brief  Handles the device state machine at each pulse
 * @retval None
 * @note Must only be called by the timer ISR
 **********************************************************/
void Motion_StepClockHandler() {
	uint32_t stepModeShift = 0;
	if (toggleOdd == 0) {
		toggleOdd = 1;
	} else {
		toggleOdd = 0;
		/* Incrementation of the relative position */
		devicePrm.relativePos += (1 << stepModeShift);

		/* Incrementation of the current position */
		if (devicePrm.direction != BACKWARD) {
			devicePrm.currentPosition += (1 << stepModeShift);
		} else {
			devicePrm.currentPosition -= (1 << stepModeShift);
		}
//		} else {
//			devicePrm.currentPosition = fIstPosition;
//		}

		switch (devicePrm.motionState) {
		case ACCELERATING: {
			uint32_t relPos = devicePrm.relativePos;
			uint32_t endAccPos = devicePrm.endAccPos;
			uint16_t speed = devicePrm.speed;
			uint32_t acc = ((uint32_t) devicePrm.acceleration << 16)
					>> stepModeShift;

			if (((devicePrm.commandExecuted & (SoftSTOP | DirChange)) != 0)
					|| ((devicePrm.commandExecuted == MOVE_CMD)	&& (relPos >= devicePrm.startDecPos))) {
				/* Apply decelerating */
				devicePrm.motionState = DECELERATING;
				devicePrm.accu = 0;

			} else if ((speed >= (devicePrm.maxSpeed >> stepModeShift))
					|| ((devicePrm.commandExecuted == MOVE_CMD)	&& (relPos >= endAccPos))) {
				/* Apply running*/
				devicePrm.motionState = STEADY;

			} else {
				/* Go on accelerating */
				uint8_t speedUpdated = 0;

				if (speed < 10)	//start Speed minimal 10 da sonst ein sprung in der Geschwindigkeit entsteht durch zu viele Schleifen der while Schleife
					speed = 10;

				devicePrm.accu += acc / speed;
				while (devicePrm.accu >= (0X10000L)) {
					devicePrm.accu -= (0X10000L);
					speed += 1;
					speedUpdated = 1;
				}
				if (speedUpdated) {
					if (speed > (devicePrm.maxSpeed >> stepModeShift)) {
						speed = devicePrm.maxSpeed >> stepModeShift;
					}
					devicePrm.speed = speed;
				}
			}
			break;
		}
		case STEADY: {
			uint16_t maxSpeed = devicePrm.maxSpeed >> stepModeShift;

			if (((devicePrm.commandExecuted & (SoftSTOP | DirChange)) != 0)
					|| ((devicePrm.commandExecuted == MOVE_CMD) && (devicePrm.relativePos >= (devicePrm.startDecPos)))
					|| ((devicePrm.commandExecuted == RUN_CMD) 	&& (devicePrm.speed > maxSpeed))) {
				/* Apply decelerating*/
				devicePrm.motionState = DECELERATING;
				devicePrm.accu = 0;

			} else if ((devicePrm.commandExecuted == RUN_CMD)
					&& (devicePrm.speed < maxSpeed)) {
				/* Apply accelerating*/
				devicePrm.motionState = ACCELERATING;
				devicePrm.accu = 0;
			}
			break;
		}
		case DECELERATING: {
			uint32_t relativePos = devicePrm.relativePos;
			uint32_t sollPos = devicePrm.stepsToTake;
			uint16_t speed = devicePrm.speed;
			uint64_t dec = ((uint32_t) devicePrm.deceleration << 16)
					>> stepModeShift;
			int32_t delta;

			if (*devicePrm.LMS) {
				relativePos = fIstPosition;
				sollPos = fZielposition;

				if (devicePrm.direction == BACKWARD)
					delta = sollPos - relativePos;
				else
					delta = relativePos - sollPos;
			} else
				delta = sollPos - relativePos;

			if ((((devicePrm.commandExecuted & (SoftSTOP | DirChange)) != 0)
					&& (speed <= (devicePrm.minSpeed >> stepModeShift))) || ((devicePrm.commandExecuted == MOVE_CMD)
					&& (delta <= 0))) {
				/* Motion process complete */
				if ((devicePrm.commandExecuted & DirChange) != 0) {
					devicePrm.commandExecuted &= ~DirChange;
					if (devicePrm.direction == BACKWARD)
						devicePrm.direction = FORWARD;
					else
						devicePrm.direction = BACKWARD;
					Motion_Board_SetDirectionGpio(devicePrm.direction);
					if ((devicePrm.commandExecuted & SoftSTOP) == 0) {
						devicePrm.motionState = ACCELERATING;
						devicePrm.accu = 0;
						break;
					}
				}
				Motion_Board_TimStckStop(0);

			} else if ((devicePrm.commandExecuted == RUN_CMD)
					&& (speed <= (devicePrm.maxSpeed >> stepModeShift))) {
				devicePrm.motionState = STEADY;
			} else {
				/* Go on decelerating */
				if (speed > (devicePrm.minSpeed >> stepModeShift)) {
					uint8_t speedUpdated = 0;
					if (speed == 0)
						speed = 1;
					devicePrm.accu += dec / speed;
					while (devicePrm.accu >= (0X10000L)) {
						devicePrm.accu -= (0X10000L);
						if (speed > 1) {
							speed -= 1;
						}
						speedUpdated = 1;
					}

					if (speedUpdated) {
						if (speed < (devicePrm.minSpeed >> stepModeShift)) {
							speed = devicePrm.minSpeed >> stepModeShift;
						}
						devicePrm.speed = speed;
					}
				}
			}
			break;
		}
		default: {
			break;
		}
		}
	}
	if ((devicePrm.motionState & INACTIVE) != INACTIVE) {
		Motion_ApplySpeed(devicePrm.speed);
	} //else {
//		if (Motion_Board_TimStckStop(&toggleOdd) == 0) {
//		}
//	}
}

/******************************************************//**
 * @brief Updates the current speed of the device
 * @param[in] newSpeed in pps
 * @retval None
 **********************************************************/
void Motion_ApplySpeed(uint16_t newSpeed) {
	if (newSpeed < Motion_MIN_STCK_FREQ) {
		newSpeed = Motion_MIN_STCK_FREQ;
	}
	if (newSpeed > Motion_MAX_STCK_FREQ) {
		newSpeed = Motion_MAX_STCK_FREQ;
	}
	devicePrm.speed = newSpeed;
	Motion_Board_TimStckSetFreq(newSpeed);
}
