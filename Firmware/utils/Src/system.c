/*----------------------------------------------------------------------------
 * Name:    system.c
 * Purpose: low level definitions
 * Note(s):
----------------------------------------------------------------------------- */ 



#include "../Inc/system.h"

//uint32_t Intervall(uint32_t min, uint32_t Value, uint32_t max)
//{
//  if (min > Value) return min;
//  if (max < Value) return max;
//  return Value;
//}

uint16_t EulerFilter(uint16_t Input, uint8_t Grad, tEuler *Buffer)
{
  int32_t Input_temp;
	int32_t Output_temp;
	if (Grad > 15) Grad = 15;
	Input_temp  = Input << Grad;
	Input_temp -= Buffer->EulerPuffer;
  Buffer->EulerPuffer  += Input_temp >> Grad;
	//Output_temp = Buffer->EulerPuffer >> Grad; 
	Output_temp = (Buffer->EulerPuffer >> Grad) + ((Buffer->EulerPuffer >> (Grad - 1)) & 1);
  return(Output_temp);
}

unsigned int calcCRC16r(unsigned int crc, unsigned int c, unsigned int mask)
{
  // LSB First (reverse)
	unsigned char i;
  for(i=0;i<8;i++)
  {
    if((crc ^ c) & 1) { crc=(crc>>1)^mask; }
    else crc>>=1;
    c>>=1;
  }
  return (crc);
}


unsigned int calcCRC8(unsigned char crc, unsigned char c, unsigned char mask)
{
  // MSB first
	unsigned char i;
  for(i=0;i<8;i++)
  {
    if((crc ^ c) & 0x80) { crc=(crc<<1)^mask; }
    else crc<<=1;
    c<<=1;
  }
  return (crc);
}

void delay_us(uint32_t time_us)
{
  SysTick->LOAD  = 72 * time_us-1;
  SysTick->VAL   = 0;                                          /* Load
the SysTick Counter Value */
  SysTick->CTRL  |= SysTick_CTRL_ENABLE_Msk;                   /* Enable
SysTick Timer */

  do
  {
  } while ((SysTick->CTRL & SysTick_CTRL_COUNTFLAG_Msk)==0);
  SysTick->CTRL  &= ~SysTick_CTRL_ENABLE_Msk;                  /*
Disable SysTick Timer */
  SysTick->VAL   = 0;                                          /* Load
the SysTick Counter Value */
}

void delay_ms(uint16_t time_ms)
{
  while (time_ms>0)
  {
    delay_us(1000);
    time_ms--;
  }
}
