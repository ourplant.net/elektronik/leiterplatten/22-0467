/*----------------------------------------------------------------------------
 * Name:    stepper.c
 * Purpose: low level definitions
 * Note(s):
 ----------------------------------------------------------------------------- */

#include "main.h"
#include "system.h"
#include <stepper.h>
#include <math.h>

tStepper *SingletonUser;

/*-----------------------------------------------------------------------------
 * Constructor
 *----------------------------------------------------------------------------*/

tStepper::tStepper(SPI_HandleTypeDef *SPI,GPIO_TypeDef* CSPort, uint16_t CS_Pin, GPIO_TypeDef* DisablePort, uint16_t DisablePin, uint32_t StepsPerTurn) {
	ResetData();
	fSPI = SPI;
	fCSPort     = CSPort;
	fCS_Pin     = CS_Pin;
	fDisablePort = DisablePort;
	fDisable_Pin = DisablePin;
	fStepsPerTurn = StepsPerTurn;

	SingletonUser = this;
}

/*-----------------------------------------------------------------------------
 * ResetData
 *----------------------------------------------------------------------------*/
void tStepper::ResetData(void) {
	fInit = false;
	fEnabled = false;
	Fehler = false;
}


/*-----------------------------------------------------------------------------
 * tmc write
 *----------------------------------------------------------------------------*/
uint32_t tStepper::tmc_write(uint32_t value)
{
	uint32_t SendBuffer = swap32(value << 8);
	uint32_t ReceiveBuffer = 0;
	uint32_t Result;
	HAL_GPIO_WritePin(fCSPort, fCS_Pin, GPIO_PIN_RESET);
	HAL_SPI_TransmitReceive(fSPI,(uint8_t*) &SendBuffer, (uint8_t*) &ReceiveBuffer, 3, 500);
	HAL_GPIO_WritePin(fCSPort, fCS_Pin, GPIO_PIN_SET);
	Result = swap32(ReceiveBuffer) >> (8+4);
	return (Result);  // tmc hat nur ein 20 bit Word statt 24
}

void tStepper::UpdateTmcStatus(void) {
	TmcStatus = tmc_write(fDRVCONF);
}

uint8_t tStepper::OpenLoad(void) {
	return (TmcStatus & ((uint32_t) 3 << OLA_POS )) != 0;
}

uint8_t tStepper::ShortToGND(void) {
	return (TmcStatus & ((uint32_t) 3 << S2GA_POS )) != 0;
}

uint8_t tStepper::TemperatureWarning(void) {
	return (TmcStatus & ((uint32_t) 1 << OTPW_POS )) != 0;
}

uint8_t tStepper::TemperatureError(void) {
	return (TmcStatus & ((uint32_t) 1 << OT_POS )) != 0;
}

uint8_t tStepper::StallGuard(void) {
	return (TmcStatus & ((uint32_t) 1 << SG_POS )) != 0;
}

uint16_t runden(double val) {
	return val + 0.5;
}
int8_t ROUND(float n) {
	float delta = (n - (int) (n));		//Zahlen vor dem Komma entfernen

	if (delta < 0)				//bei negativen Zahlen das vorzeichen entfernen
		delta *= -1;

	if (delta >= 0.5)
		if (n > 0)
			return (int) (n + 1);
		else
			return (int) (n - 1);
	else
		return (int) (n);
}

void tStepper::tmc_DRVCTRL(uint32_t value) {
	uint32_t Data;
	Data = (DRVCTRL ) | (value & (uint32_t) 0x2FFFF);
	if (Data != fDRVCTRL) {
		fDRVCTRL = Data;
		tmc_write(Data);
	}
}

void tStepper::tmc_CHOPCONF(uint32_t value) {
	uint32_t Data;
	Data = (CHOPCONF ) | (value & (uint32_t) 0x1FFFF);
	if (Data != fCHOPCONF) {
		fCHOPCONF = Data;
		tmc_write(Data);
	}
}
void tStepper::tmc_SMARTEN(uint32_t value) {
	uint32_t Data;
	Data = (SMARTEN ) | (value & (uint32_t) 0x0FFFF);
	if (Data != fSMARTEN) {
		fSMARTEN = Data;
		tmc_write(Data);
	}
}

void tStepper::tmc_SGCSCONF(uint32_t value) {
	uint32_t Data;
	Data = (SGCSCONF ) | (value & (uint32_t) 0x1FFFF);
	if (Data != fSGCSCONF) {
		fSGCSCONF = Data;
		tmc_write(Data);
	}
}

void tStepper::StallGuardThreshold(int8_t value) {
	uint32_t Data;
	Data = (fSGCSCONF & 0xFF00FF) | ((value & 0x7F) << SGT_POS );
	tmc_SGCSCONF(Data);
}

void tStepper::tmc_RDSEL(uint8_t value) {
	uint32_t vOLD = (fDRVCONF >> RDSEL_POS ) & (uint32_t) 3;
	if ((value < 3) && (value != vOLD))
		tmc_DRVCONF(
				(fDRVCONF & ~((uint32_t) 3 << RDSEL_POS ))
						| (value << RDSEL_POS ));
}

void tStepper::tmc_DRVCONF(uint32_t value) {
	uint32_t Data;
	Data = (DRVCONF ) | (value & (uint32_t) 0x1FFFF);
	fDRVCONF = Data;
	tmc_write(Data);
}

void tStepper::tmc_init(double current) {
	uint32_t Data;

	uint32_t SenseResistorVoltage;
	uint16_t Interpolation, Parameter;
	double vTemp;

#define MessShunt 0.15     // Ohm
#define VSENSE_1  0.165   // V
#define VSENSE_0  0.310   // V

	vTemp = 32 * current * (254/248) * sqrt(2);				//cs =  runden((current*(254/248)*(MessShunt/VSENSE_1)*sqrt(2)*32)-1);
	cs =  runden((vTemp*(MessShunt/VSENSE_1))-1);
	if (cs > 31) {
		cs =  runden((vTemp*(MessShunt/VSENSE_0))-1);
		SenseResistorVoltage = 0; // 0.310
		if (cs > 31)
			cs = 31;
	} else
		SenseResistorVoltage = (uint32_t) 1 << VSENSE_POS; // 0.165
	cs = cs << CS_POS;

	// CHOPCONF
#define BlankTimeSelect_16_clocks  (uint32_t) 0
#define BlankTimeSelect_24_clocks  (uint32_t) 1
#define BlankTimeSelect_36_clocks  (uint32_t) 2
#define BlankTimeSelect_54_clocks  (uint32_t) 3

#define HysteresisLowValue_0 			 (uint32_t) 3
#define HysteresisLowValue_1 			 (uint32_t) 4
#define HysteresisLowValue_2 			 (uint32_t) 5
#define HysteresisLowValue_3 			 (uint32_t) 6

#define HysteresisStartValue_add_1 (uint32_t) 0
#define HysteresisStartValue_add_2 (uint32_t) 1
#define HysteresisStartValue_add_3 (uint32_t) 2
#define HysteresisStartValue_add_4 (uint32_t) 3

#define OffTimeAndDriverEnable_DIS (uint32_t) 0  // Driver disable, all bridges off
#define OffTimeAndDriverEnable_2   (uint32_t) 2
#define OffTimeAndDriverEnable_3   (uint32_t) 3
#define OffTimeAndDriverEnable_4   (uint32_t) 4
#define OffTimeAndDriverEnable_5   (uint32_t) 5

	Data = (CHOPCONF ) | (BlankTimeSelect_36_clocks << TBL_POS )
			| (HysteresisLowValue_0 << HEND_POS )
			| (HysteresisStartValue_add_4 << HSTRT_POS )
			| (OffTimeAndDriverEnable_4 << TOFF_POS );
	tmc_write(Data);

	// SGCSCONF  

//#define StallGuardThresholdValue   (uint32_t) -1 << SGT_POS      // -64 to 63  A higher value makes stallGuard less sensitive
	StallGuardThresholdValue = 0 << SGT_POS;
#define CurrentScale_1div32  			 (uint32_t) 0 << CS_POS
#define CurrentScale_2div32  			 (uint32_t) 1 << CS_POS
#define CurrentScale_3div32  			 (uint32_t) 2 << CS_POS
#define CurrentScale_4div32  			 (uint32_t) 3 << CS_POS
#define CurrentScale_5div32  			 (uint32_t) 4 << CS_POS
#define CurrentScale_16div32  		 (uint32_t) 15 << CS_POS

	tmc_SGCSCONF(StallGuardFilterEnable | StallGuardThresholdValue | cs);

	// DRVCONF  
#define ReservedTestMode					 (uint32_t)1 << TST_POS
#define SenseResistorVoltage_305mV (uint32_t)0 << VSENSE_POS
#define SenseResistorVoltage_165mV (uint32_t)1 << VSENSE_POS

#define SelectValueForRead_MicrostepPos      (uint32_t)0 << RDSEL_POS
#define SelectValueForRead_StallGuardLevel   (uint32_t)1 << RDSEL_POS
#define SelectValueForRead_SGandSmartCurrent (uint32_t)2 << RDSEL_POS
	tmc_DRVCONF(SenseResistorVoltage | SelectValueForRead_StallGuardLevel);

	// DRVCTRL  

	Interpolation = fStepsPerTurn;
	switch (Interpolation) {
	case 1:
		Parameter = MicroStepResolution_FULLSTEP;
		break;
	case 2:
		Parameter = MicroStepResolution_2;
		break;
	case 4:
		Parameter = MicroStepResolution_4;
		break;
	case 8:
		Parameter = MicroStepResolution_8;
		break;
	case 16:
		Parameter = MicroStepResolution_16;
		break;
	case 32:
		Parameter = MicroStepResolution_32;
		break;
	case 64:
		Parameter = MicroStepResolution_64;
		break;
	case 128:
		Parameter = MicroStepResolution_128;
		break;
	case 256:
		Parameter = MicroStepResolution_256;
		break;

	default:
		Parameter = MicroStepResolution_FULLSTEP;
		break;
	}

#define MicroStepInterpolation_ON			(uint32_t) 1  // Only  in Resolution 16x microsteps
#define MicroStepInterpolation_OFF  	(uint32_t) 0

	//Data = (DRVCTRL)  | (MicroStepInterpolation_ON << INTPOL_POS) | (Parameter << MRES_POS); // write 0xxx, SDOFF=0
	Data = (DRVCTRL ) | (MicroStepInterpolation_ON << INTPOL_POS )
			| (Parameter << MRES_POS ); // write 0xxx, SDOFF=0
	tmc_write(Data);
}

void tStepper::SetSGCSconf(uint16_t speed) {

	int8_t resault = ROUND(StallGuardAnstieg * speed + StallGuardOffset);
	if (resault > 3)
		resault = 3;
	if (resault < -13)
		resault =-13;

	StallGuardThresholdValue = resault << SGT_POS;
	tmc_SGCSCONF(StallGuardFilterEnable | StallGuardThresholdValue | cs);
}

void tStepper::tmc_denit(void) {
	uint32_t Data;
	Data = CHOPCONF;
	tmc_write(Data);
}

void tStepper::Enable(void)
{
  HAL_GPIO_WritePin(fDisablePort, fDisable_Pin, GPIO_PIN_RESET);
	fEnabled = true;
}

void tStepper::Disable(void)
{
  HAL_GPIO_WritePin(fDisablePort, fDisable_Pin, GPIO_PIN_SET);
	fEnabled = false;
}

uint8_t tStepper::Enabled(void) {
	return fEnabled;
}
