#ifndef __MEM_QUEUE_HPP
#define __MEM_QUEUE_HPP

#if defined(STM32F767xx)
#include <stm32f7xx.h>
#else
#include <stm32f4xx.h>
#endif

#define maxQueue  32

typedef struct
{
  uint8_t  Adress;
  uint16_t Data;
} tMem_Dataset;


class tMem_queue
{
private:
    //const int maxQueue = 256; // Konstante f�r die Queuegr��e
    //tMem_Dataset *queue;          // Die Queue selbst
    int head, tail;
    tMem_Dataset queue[maxQueue];
public:
    tMem_queue()  // wird bei Definition festgelegt
        { head = tail = 0;}
    // ~tMem_queue() {delete[] queue;}
    bool deq(tMem_Dataset *get); // abholen
    bool q(tMem_Dataset num);    // drauflegen
    bool empty(void) {return(head == tail);};    // drauflegen
};


#endif

