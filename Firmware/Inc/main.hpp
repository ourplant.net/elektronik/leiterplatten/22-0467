/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

  /* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

#include <stm32f4xx.h>

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

void HAL_TIM_MspPostInit(TIM_HandleTypeDef *htim);

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */
void MsTick_Handler(void);
/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define CFO1_Pin GPIO_PIN_13
#define CFO1_GPIO_Port GPIOC
#define NIN1_Pin GPIO_PIN_14
#define NIN1_GPIO_Port GPIOC
#define OE1_Pin GPIO_PIN_15
#define OE1_GPIO_Port GPIOC
#define VCC_Motor_Pin GPIO_PIN_0
#define VCC_Motor_GPIO_Port GPIOC
#define SSI_CLK_Pin GPIO_PIN_5
#define SSI_CLK_GPIO_Port GPIOA
#define SSI_DAT_Pin GPIO_PIN_6
#define SSI_DAT_GPIO_Port GPIOA
#define CFO4_Pin GPIO_PIN_4
#define CFO4_GPIO_Port GPIOC
#define NIN4_Pin GPIO_PIN_5
#define NIN4_GPIO_Port GPIOC
#define LED_D6_Pin GPIO_PIN_0
#define LED_D6_GPIO_Port GPIOB
#define LED_D3_Pin GPIO_PIN_1
#define LED_D3_GPIO_Port GPIOB
#define iBOOT1_Pin GPIO_PIN_2
#define iBOOT1_GPIO_Port GPIOB
#define SPI2_CS_Pin GPIO_PIN_12
#define SPI2_CS_GPIO_Port GPIOB
#define OE4_Pin GPIO_PIN_6
#define OE4_GPIO_Port GPIOC
#define CFO3_Pin GPIO_PIN_7
#define CFO3_GPIO_Port GPIOC
#define NIN3_Pin GPIO_PIN_8
#define NIN3_GPIO_Port GPIOC
#define OE3_Pin GPIO_PIN_9
#define OE3_GPIO_Port GPIOC
#define Disable_Pin GPIO_PIN_10
#define Disable_GPIO_Port GPIOA
#define DIR_Pin GPIO_PIN_11
#define DIR_GPIO_Port GPIOA
#define CFO2_Pin GPIO_PIN_10
#define CFO2_GPIO_Port GPIOC
#define NIN2_Pin GPIO_PIN_11
#define NIN2_GPIO_Port GPIOC
#define OE2_Pin GPIO_PIN_12
#define OE2_GPIO_Port GPIOC
#define ST25_GPO_Pin GPIO_PIN_2
#define ST25_GPO_GPIO_Port GPIOD
#define STEP_Pin GPIO_PIN_5
#define STEP_GPIO_Port GPIOB
#define EEPROM_SCL_Pin GPIO_PIN_6
#define EEPROM_SCL_GPIO_Port GPIOB
#define EEPROM_SDA_Pin GPIO_PIN_7
#define EEPROM_SDA_GPIO_Port GPIOB

/* USER CODE BEGIN Private defines */

// FPGA CONFIG
#define ENCDIR_DR_BIT 	  0x05      // Entsprechendedes BIT IM FPGA
#define ENCDIR_LR_BIT 	  0x06      // Entsprechendedes BIT IM FPGA
#define Regelsinn_BIT 	  0x07      // Entsprechendedes BIT IM FPGA
#define LMS_EN_BIT    	  0x08      // Entsprechendedes BIT IM FPGA
#define REF_POL_BIT    	  0x09      // Wird von der Software Ausgewertet und im FPGA ignoriert
#define INDEX_EN_BIT  	  0x0A      // Wird von der Software Ausgewertet und im FPGA ignoriert
#define INDEX_POL_BIT  	  0x0B      // Wird von der Software Ausgewertet und im FPGA ignoriert
#define HWES_EN           0x0C      // Entsprechendedes BIT IM FPGA
#define CLR_POS_BIT       0x0F      // Entsprechendedes BIT IM FPGA Löscht das Register der Incrementalencoders

#define VPlusMin 	14
#define VPlus48		42
/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
