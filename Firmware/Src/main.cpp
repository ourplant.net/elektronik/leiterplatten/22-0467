/* USER CODE BEGIN Header */
/**
 ******************************************************************************
 * @file           : main.c
 * @brief          : Main program body
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
 * All rights reserved.</center></h2>
 *
 * This software component is licensed by ST under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 ******************************************************************************
 */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "math.h"
#include <hal_unican.h>
#include <hal_IIC_EEPROM.h>
#include <Motion.h>
#include "mem_queue.h"
#include "system.h"
#include "stepper.h"
#include "BISS_C.h"

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
#define Default_NODE_ID 19
#define NAME        "STAX"
#define SW_VERSION  "V2.4"
#define HW_VERSION  "V1.0"

#define MOTORSTEPS              16

#define DEFAULT_ENCDIR_DR          0
#define DEFAULT_ENCDIR_LR          0
#define DEFAULT_Regelsinn          0
#define DEFAULT_LMS_enable         0
#define DEFAULT_REFPolaritaet      0
#define DEFAULT_IndexReferenz      0           // Index auf Ref enable
#define DEFAULT_IndexPolaritaet    0           // Index auf Ref HI
#define DEFAULT_HWESE              0           // HW_Endschalter_enabled
#define DEFAULT_LRP  0
#define DEFAULT_DRP  0
#define DEFAULT_DRTI  0
#define DEFAULT_DRTD  0 // TI
#define DEFAULT_TII    0
#define DEFAULT_IMIN   0
#define DEFAULT_IMAX   1000
#define MAX_I 		   1450
#define DEFAULT_VSOLL  0
#define DEFAULT_ASOLL  0
#define DEFAULT_COUNTER 500

#define Warparound_Filterslide_Pos 1

#define DEFAULT_CONFIG  ((DEFAULT_ENCDIR_DR <<  ENCDIR_DR_BIT) | \
                         (DEFAULT_ENCDIR_LR <<  ENCDIR_LR_BIT) | \
                         (DEFAULT_Regelsinn <<  Regelsinn_BIT) | \
                         (DEFAULT_LMS_enable << LMS_EN_BIT) | \
                         (DEFAULT_REFPolaritaet <<  REF_POL_BIT) | \
                         (DEFAULT_IndexReferenz <<  INDEX_EN_BIT) | \
                         (DEFAULT_ENCDIR_LR <<  INDEX_POL_BIT) | \
                         (DEFAULT_IndexPolaritaet <<  CLR_POS_BIT) | \
                         (DEFAULT_HWESE << HWES_EN));

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
ADC_HandleTypeDef hadc1;
DMA_HandleTypeDef hdma_adc1;
CAN_HandleTypeDef hcan1;
I2C_HandleTypeDef hi2c1;
SPI_HandleTypeDef hspi1;
SPI_HandleTypeDef hspi2;
DMA_HandleTypeDef hdma_spi1_rx;
DMA_HandleTypeDef hdma_spi1_tx;
TIM_HandleTypeDef htim3;		// zur Takterzeugung f�r Motor

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/

tUnican *pUNICAN;
uint16_t CAN_Controlword;
uint16_t CAN_Statusword;
uint8_t calibrateEnable = 0;

uint8_t FPGA_Mirror[0x200];
uint16_t Sollspeed, Acceleration;
uint8_t InPosition;
uint8_t LMSenable;
int32_t fZielposition;
int32_t fIstPosition;
int32_t fSchleppfehler;
uint32_t fDelay[10];
int32_t deltaPos;
uint16_t VPLUS_VALUE;
uint16_t ADC1ConvertedValues[1];
uint8_t Error;
uint8_t fFlagDauerbetrieb = 0;

extern tEEPROM_Registers *EManReg;
tSERVO_EEPROM_Default Achse0;
tStepper Stepper1(&hspi2, SPI2_CS_GPIO_Port, SPI2_CS_Pin, Disable_GPIO_Port,
Disable_Pin, MOTORSTEPS);

volatile uint8_t Statusled;
uint16_t counter_StatusLED = 0;
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_DMA_Init(void);
static void MX_ADC1_Init(void);
static void MX_CAN1_Init(void);
static void MX_I2C1_Init(void);
static void MX_SPI1_Init(void);
static void MX_SPI2_Init(void);
static void MX_TIM3_Init(void);
/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/

//static void Sleep(uint32_t ms);
void SetPin(GPIO_TypeDef *GPIOx, uint16_t GPIO_Pin, uint8_t PinState);
void GetPosition(void);
void Handle_Controlword(void);
void Handle_Statusword(void);
void LoadParameters(void);
void writeEEPROM(uint8_t Register, uint32_t data);

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
 tStepper *pStepper;
 tServoEEPROM ServoEEPROM(EEpromMirror_Size);
 tBiss *pMesssystem;
 uint8_t dataRx[8];

/* USER CODE END 0 */

/**
 * @brief  The application entry point.
 * @retval int
 */
int main(void)
{
	/* USER CODE BEGIN 1 */
	HAL_DeInit();
	int8_t TPDO_State = 0;
	/* USER CODE END 1 */

	/* MCU Configuration--------------------------------------------------------*/

	/* Reset of all peripherals, Initializes the Flash interface and the Systick. */
	HAL_Init();

	/* USER CODE BEGIN Init */

	/* USER CODE END Init */

	/* Configure the system clock */
	SystemClock_Config();

	/* USER CODE BEGIN SysInit */
	HAL_Delay(50);
	/* USER CODE END SysInit */

	/* Initialize all configured peripherals */
	MX_GPIO_Init();
	MX_DMA_Init();
	MX_ADC1_Init();
	MX_CAN1_Init();
	MX_I2C1_Init();
	MX_SPI1_Init();
	MX_SPI2_Init();
	MX_TIM3_Init();
	/* USER CODE BEGIN 2 */
    tBiss Messsystem(&hspi1);
    pMesssystem = &Messsystem;

	ServoEEPROM.ConnectSerialEEPROM(&hi2c1);
	tUnican UNICAN(&hcan1, &ServoEEPROM);
	pUNICAN = &UNICAN;

	LoadParameters();

	pStepper = &Stepper1;
	Stepper1.tmc_init(0.2);
	HAL_Delay(1);

	pStepper->Disable();
	HAL_Delay(1);
	Motion_Init(&LMSenable, &Messsystem.andersrum, &Messsystem.IncPerStep);
	Motion_SetMinSpeed(1);

    if (hdma_adc1.State == HAL_DMA_STATE_READY)	
	  HAL_ADC_Start_DMA(&hadc1, (uint32_t*) ADC1ConvertedValues, 1);

	/* USER CODE END 2 */

	/* Infinite loop */
	/* USER CODE BEGIN WHILE */
	while (1) {
		/* USER CODE END WHILE */
	  CAN_Controlword = 0;
		/* USER CODE BEGIN 3 */
		UNICAN.Kaltstart_request = false;
		UNICAN.Reset_Communication = true;

		while (!UNICAN.Handle_Coldstart()) {
			if (!UNICAN.Kaltstart_request && UNICAN.Reset_Communication) {
				UNICAN.OpenCan(Default_NODE_ID, NAME, HW_VERSION, SW_VERSION);
				UNICAN.Reset_Communication = false;
				UNICAN.RPDO[0][1] = (UNICAN.RPDO[0][0] =
						(uint8_t*) &CAN_Controlword) + 1;
				UNICAN.RPDO[0][1] = (UNICAN.RPDO[0][0] =
						(uint8_t*) &CAN_Controlword) + 1;

				UNICAN.RPDO[1][3] =
						(UNICAN.RPDO[1][2] = (UNICAN.RPDO[1][1] =
								(UNICAN.RPDO[1][0] = (uint8_t*) &fZielposition)
										+ 1) + 1) + 1;
				UNICAN.RPDO[1][5] = (UNICAN.RPDO[1][4] = (uint8_t*) &Sollspeed)
						+ 1;
				UNICAN.RPDO[1][7] = (UNICAN.RPDO[1][6] =
						(uint8_t*) &Acceleration) + 1;
			}

			if (Sollspeed != Motion_GetSpeed() and Sollspeed > 0)
				Motion_SetMaxSpeed(Sollspeed);

			if (Acceleration != Motion_GetAcceleration() and Acceleration > 0)
				Motion_SetAcceleration(Acceleration);

// 			StallGuard �berwachen
			pStepper->UpdateTmcStatus();
			if (pStepper->StallGuard())
				HAL_GPIO_WritePin(LED_D6_GPIO_Port, LED_D6_Pin, GPIO_PIN_RESET);
			else
				HAL_GPIO_WritePin(LED_D6_GPIO_Port, LED_D6_Pin, GPIO_PIN_SET);

//			StallGuard berechnen
			if (pStepper->Enabled()) {
				pStepper->SetSGCSconf(Motion_GetSpeed());
			}

			GetPosition();

			/*48V ADC messen */
			VPLUS_VALUE = ADC1ConvertedValues[0] / 64;
		
			switch (TPDO_State) {
			case 3:
				if (UNICAN.Handle_TPDO(1, 0, 4, CAN_Statusword,
						*(((uint8_t*) &CAN_Statusword) + 1),
						UNICAN.Last_CAN_Error,
						*(((uint8_t*) &UNICAN.Last_CAN_Error) + 1), 0, 0, 0, 0))
					/*	*(((uint8_t*) &fSchleppfehler) + 0),
						            *(((uint8_t*) &fSchleppfehler) + 1),
						            *(((uint8_t*) &fSchleppfehler) + 2),
						            *(((uint8_t*) &fSchleppfehler) + 3)))*/
					;
				TPDO_State++;
				break;
			case 2:
				int32_t StepSpeed;
				StepSpeed = Motion_GetSpeed();
				if (UNICAN.Handle_TPDO(2, 1, 8,
						*(((uint8_t*) &fIstPosition) + 0),
						*(((uint8_t*) &fIstPosition) + 1),
						*(((uint8_t*) &fIstPosition) + 2),
						*(((uint8_t*) &fIstPosition) + 3),
						*(((uint8_t*) &StepSpeed) + 0),
						*(((uint8_t*) &StepSpeed) + 1),
						*(((uint8_t*) &StepSpeed) + 2),
						*(((uint8_t*) &StepSpeed) + 3)))
					;
				TPDO_State++;
				break;
			case 1:
				if (UNICAN.Handle_TPDO(3, 1, 4,
						*(((uint8_t*) &pStepper->TmcStatus) + 0), 0,
						(uint8_t) (pStepper->TmcStatus >> 10),
						(uint8_t) (pStepper->TmcStatus >> 18), /**(((uint8_t*) &pStepper->StallGuardThresholdValue) + 1)*/
						0, 0, 0, 0))
					;
				TPDO_State++;
				break;
			default:
				if (TPDO_State > 3)
					TPDO_State = 0;
				TPDO_State++;
				break;

			}

			if (Statusled) //STM32 works
				HAL_GPIO_WritePin(LED_D3_GPIO_Port, LED_D3_Pin, GPIO_PIN_SET);
			else
				HAL_GPIO_WritePin(LED_D3_GPIO_Port, LED_D3_Pin, GPIO_PIN_RESET);

			Handle_Statusword();
			Handle_Controlword();
			UNICAN.Handle_Emergency();
		}
	}
	/* USER CODE END 3 */
}

/**
 * @brief System Clock Configuration
 * @retval None
 */
void SystemClock_Config(void)
{
	RCC_OscInitTypeDef RCC_OscInitStruct = { 0 };
	RCC_ClkInitTypeDef RCC_ClkInitStruct = { 0 };

	/**Configure the main internal regulator output voltage
	 */
    __HAL_RCC_PWR_CLK_ENABLE();
	__HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);

	/**Initializes the CPU, AHB and APB busses clocks
	 */
	RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
	RCC_OscInitStruct.HSEState = RCC_HSE_ON;
	RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
	RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
	RCC_OscInitStruct.PLL.PLLM = 4;
	RCC_OscInitStruct.PLL.PLLN = 168;
	RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
	RCC_OscInitStruct.PLL.PLLQ = 4;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
		Error_Handler();
	}
	/**Initializes the CPU, AHB and APB busses clocks
	 */
	RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_SYSCLK
			| RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2;
	RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_5) != HAL_OK)
  {
		Error_Handler();
	}
}

/**
 * @brief ADC1 Initialization Function
 * @param None
 * @retval None
 */
static void MX_ADC1_Init(void)
{

  /* USER CODE BEGIN ADC1_Init 0 */

  /* USER CODE END ADC1_Init 0 */

  ADC_ChannelConfTypeDef sConfig = { 0 };

  /* USER CODE BEGIN ADC1_Init 1 */

  /* USER CODE END ADC1_Init 1 */
  /** Configure the global features of the ADC (Clock, Resolution, Data Alignment and number of conversion)
   */
  hadc1.Instance = ADC1;
  hadc1.Init.ClockPrescaler = ADC_CLOCK_SYNC_PCLK_DIV8;
  hadc1.Init.Resolution = ADC_RESOLUTION_12B;
  hadc1.Init.ScanConvMode = ENABLE;
  hadc1.Init.ContinuousConvMode = ENABLE;
  hadc1.Init.DiscontinuousConvMode = DISABLE;
  hadc1.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
  hadc1.Init.ExternalTrigConv = ADC_SOFTWARE_START;
  hadc1.Init.DataAlign = ADC_DATAALIGN_RIGHT;
  hadc1.Init.NbrOfConversion = 1;
  hadc1.Init.DMAContinuousRequests = ENABLE;
  hadc1.Init.EOCSelection = ADC_EOC_SEQ_CONV;
  if (HAL_ADC_Init(&hadc1) != HAL_OK)
  {
		Error_Handler();
	}
	/** Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time.
	 */
	sConfig.Channel = ADC_CHANNEL_10;
	sConfig.Rank = 1;
  sConfig.SamplingTime = ADC_SAMPLETIME_480CYCLES;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
		Error_Handler();
	}
	/* USER CODE BEGIN ADC1_Init 2 */

	/* USER CODE END ADC1_Init 2 */

}

/**
 * @brief CAN1 Initialization Function
 * @param None
 * @retval None
 */
static void MX_CAN1_Init(void)
{

	/* USER CODE BEGIN CAN1_Init 0 */

	/* USER CODE END CAN1_Init 0 */

	/* USER CODE BEGIN CAN1_Init 1 */

	/* USER CODE END CAN1_Init 1 */
	hcan1.Instance = CAN1;
	hcan1.Init.Prescaler = 42;
	hcan1.Init.Mode = CAN_MODE_NORMAL;
    hcan1.Init.SyncJumpWidth = CAN_SJW_3TQ;
	hcan1.Init.TimeSeg1 = CAN_BS1_5TQ;
	hcan1.Init.TimeSeg2 = CAN_BS2_2TQ;
	hcan1.Init.TimeTriggeredMode = DISABLE;
	hcan1.Init.AutoBusOff = ENABLE;
	hcan1.Init.AutoWakeUp = DISABLE;
	hcan1.Init.AutoRetransmission = ENABLE;
	hcan1.Init.ReceiveFifoLocked = DISABLE;
	hcan1.Init.TransmitFifoPriority = DISABLE;
  if (HAL_CAN_Init(&hcan1) != HAL_OK)
  {
		Error_Handler();
	}
	/* USER CODE BEGIN CAN1_Init 2 */

	/* USER CODE END CAN1_Init 2 */

}

/**
 * @brief I2C1 Initialization Function
 * @param None
 * @retval None
 */
static void MX_I2C1_Init(void)
{

	/* USER CODE BEGIN I2C1_Init 0 */

	/* USER CODE END I2C1_Init 0 */

	/* USER CODE BEGIN I2C1_Init 1 */

	/* USER CODE END I2C1_Init 1 */
	hi2c1.Instance = I2C1;
	hi2c1.Init.ClockSpeed = 100000;
	hi2c1.Init.DutyCycle = I2C_DUTYCYCLE_2;
	hi2c1.Init.OwnAddress1 = 0;
	hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
	hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
	hi2c1.Init.OwnAddress2 = 0;
	hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
	hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
    if (HAL_I2C_Init(&hi2c1) != HAL_OK)
    {
		Error_Handler();
	}
	/* USER CODE BEGIN I2C1_Init 2 */

	/* USER CODE END I2C1_Init 2 */

}

/**
 * @brief SPI1 Initialization Function
 * @param None
 * @retval None
 */
static void MX_SPI1_Init(void)
{

	/* USER CODE BEGIN SPI1_Init 0 */

	/* USER CODE END SPI1_Init 0 */

	/* USER CODE BEGIN SPI1_Init 1 */

	/* USER CODE END SPI1_Init 1 */
	/* SPI1 parameter configuration*/
	hspi1.Instance = SPI1;
	hspi1.Init.Mode = SPI_MODE_MASTER;
	hspi1.Init.Direction = SPI_DIRECTION_2LINES;
	hspi1.Init.DataSize = SPI_DATASIZE_8BIT;
	hspi1.Init.CLKPolarity = SPI_POLARITY_LOW;
	hspi1.Init.CLKPhase = SPI_PHASE_1EDGE;
	hspi1.Init.NSS = SPI_NSS_SOFT;
	hspi1.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_32;
	hspi1.Init.FirstBit = SPI_FIRSTBIT_MSB;
	hspi1.Init.TIMode = SPI_TIMODE_DISABLE;
	hspi1.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
	hspi1.Init.CRCPolynomial = 10;
    if (HAL_SPI_Init(&hspi1) != HAL_OK)
    {
		Error_Handler();
	}
	/* USER CODE BEGIN SPI1_Init 2 */

	/* USER CODE END SPI1_Init 2 */

}

/**
 * @brief SPI2 Initialization Function
 * @param None
 * @retval None
 */
static void MX_SPI2_Init(void)
{

	/* USER CODE BEGIN SPI2_Init 0 */

	/* USER CODE END SPI2_Init 0 */

	/* USER CODE BEGIN SPI2_Init 1 */

	/* USER CODE END SPI2_Init 1 */
	/* SPI2 parameter configuration*/
	hspi2.Instance = SPI2;
	hspi2.Init.Mode = SPI_MODE_MASTER;
	hspi2.Init.Direction = SPI_DIRECTION_2LINES;
	hspi2.Init.DataSize = SPI_DATASIZE_8BIT;
	hspi2.Init.CLKPolarity = SPI_POLARITY_HIGH;
	hspi2.Init.CLKPhase = SPI_PHASE_2EDGE;
	hspi2.Init.NSS = SPI_NSS_SOFT;
	hspi2.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_32;
	hspi2.Init.FirstBit = SPI_FIRSTBIT_MSB;
	hspi2.Init.TIMode = SPI_TIMODE_DISABLE;
	hspi2.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
	hspi2.Init.CRCPolynomial = 10;
    if (HAL_SPI_Init(&hspi2) != HAL_OK)
    {
		Error_Handler();
	}
	/* USER CODE BEGIN SPI2_Init 2 */

	/* USER CODE END SPI2_Init 2 */

}

/**
 * @brief TIM3 Initialization Function
 * @param None
 * @retval None
 */
static void MX_TIM3_Init(void)
{

	/* USER CODE BEGIN TIM3_Init 0 */

	/* USER CODE END TIM3_Init 0 */

	TIM_MasterConfigTypeDef sMasterConfig = { 0 };
	TIM_OC_InitTypeDef sConfigOC = { 0 };

	/* USER CODE BEGIN TIM3_Init 1 */

	/* USER CODE END TIM3_Init 1 */
	htim3.Instance = TIM3;
	htim3.Init.Prescaler = 0;
	htim3.Init.CounterMode = TIM_COUNTERMODE_UP;
	htim3.Init.Period = 0;
	htim3.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
	htim3.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
    if (HAL_TIM_OC_Init(&htim3) != HAL_OK)
    {
		Error_Handler();
	}
	sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
	sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
    if (HAL_TIMEx_MasterConfigSynchronization(&htim3, &sMasterConfig) != HAL_OK)
    {
		Error_Handler();
	}
	sConfigOC.OCMode = TIM_OCMODE_TIMING;
	sConfigOC.Pulse = 0;
	sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
	sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
    if (HAL_TIM_OC_ConfigChannel(&htim3, &sConfigOC, TIM_CHANNEL_2) != HAL_OK)
    {
		Error_Handler();
	}
	/* USER CODE BEGIN TIM3_Init 2 */

	/* USER CODE END TIM3_Init 2 */
	HAL_TIM_MspPostInit(&htim3);

}

/**
 * Enable DMA controller clock
 */
static void MX_DMA_Init(void) 
{

	/* DMA controller clock enable */
  __HAL_RCC_DMA2_CLK_ENABLE();

	/* DMA interrupt init */
	/* DMA2_Stream0_IRQn interrupt configuration */
	HAL_NVIC_SetPriority(DMA2_Stream0_IRQn, 5, 0);
	HAL_NVIC_EnableIRQ(DMA2_Stream0_IRQn);
  /* DMA2_Stream2_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA2_Stream2_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA2_Stream2_IRQn);
  /* DMA2_Stream3_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA2_Stream3_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA2_Stream3_IRQn);

}

/**
 * @brief GPIO Initialization Function
 * @param None
 * @retval None
 */
static void MX_GPIO_Init(void)
{
	GPIO_InitTypeDef GPIO_InitStruct = { 0 };

	/* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();

	/*Configure GPIO pin Output Level */
	HAL_GPIO_WritePin(GPIOC, NIN1_Pin | OE1_Pin | NIN4_Pin | OE4_Pin 
			| NIN3_Pin | OE3_Pin | NIN2_Pin | OE2_Pin, GPIO_PIN_RESET);

	/*Configure GPIO pin Output Level */
	HAL_GPIO_WritePin(GPIOB, LED_D6_Pin | LED_D3_Pin | SPI2_CS_Pin,GPIO_PIN_RESET);

	/*Configure GPIO pin Output Level */
	HAL_GPIO_WritePin(GPIOA, Disable_Pin | DIR_Pin, GPIO_PIN_RESET);

	/*Configure GPIO pins : CFO1_Pin CFO4_Pin CFO3_Pin CFO2_Pin */
	GPIO_InitStruct.Pin = CFO1_Pin | CFO4_Pin | CFO3_Pin | CFO2_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull = GPIO_PULLUP;
	HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

	/*Configure GPIO pins : NIN1_Pin OE1_Pin NIN4_Pin OE4_Pin
	 NIN3_Pin OE3_Pin NIN2_Pin OE2_Pin */
	GPIO_InitStruct.Pin = NIN1_Pin | OE1_Pin | NIN4_Pin | OE4_Pin 
							| NIN3_Pin| OE3_Pin | NIN2_Pin | OE2_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

	/*Configure GPIO pins : LED_D6_Pin LED_D3_Pin SPI2_CS_Pin */
	GPIO_InitStruct.Pin = LED_D6_Pin | LED_D3_Pin | SPI2_CS_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

	/*Configure GPIO pin : iBOOT1_Pin */
	GPIO_InitStruct.Pin = iBOOT1_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(iBOOT1_GPIO_Port, &GPIO_InitStruct);

	/*Configure GPIO pins : Disable_Pin DIR_Pin */
	GPIO_InitStruct.Pin = Disable_Pin | DIR_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
	/*Configure GPIO pin : ST25_GPO_Pin */
	GPIO_InitStruct.Pin = ST25_GPO_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(ST25_GPO_GPIO_Port, &GPIO_InitStruct);
}

/* USER CODE BEGIN 4 */

void BeforeFlashloader(void)
{
  HAL_ADC_Stop_DMA(&hadc1);
  HAL_SPI_DMAStop(&hspi1);
  Motion_Board_TimStckStop(0);
}
void MsTick_Handler(void)
{
	if(pUNICAN != NULL)
		pUNICAN->Call_CIA_TIMERS();

	if (counter_StatusLED > 0)
		counter_StatusLED--;
	else {
		counter_StatusLED = DEFAULT_COUNTER;
		Statusled = !Statusled;
	}
}


void Handle_Controlword(void) {
	uint16_t Controlword_temp;
//	uint16_t Controlword_Set_Back;

	Controlword_temp = CAN_Controlword;

	// ------------------------------------------------
	// CAN_Controlword
	// ------------------------------------------------
//	Controlword_Set_Back = 0xFFFF;

	//Fahr auf Position
	if (((Controlword_temp) & 0x0001) != 0) {
		if (VPLUS_VALUE > VPlusMin) {
			Stepper1.Fehler = 0;
			if (!pStepper->Enabled()) {			// einschalten und auf aktueller Position bleiben
				GetPosition();
				fZielposition = fIstPosition;
				Stepper1.tmc_init(Achse0.IMAX / 1000.0);
				pStepper->Enable();
			}
			if (!Motion_GetAktive() and !fFlagDauerbetrieb) {
				Motion_SetMaxSpeed(Sollspeed);
				Motion_SetAcceleration(Acceleration);
				GetPosition();
				Motion_GoTo(fZielposition, fIstPosition);
			}
		} else
			Stepper1.Fehler = 1;
	}
	//Dauerbetrieb
	if (((Controlword_temp) & 0x0002) != 0) {
		if (VPLUS_VALUE > VPlusMin) {
			Stepper1.Fehler = 0;
			if (pStepper->Enabled() and !Motion_GetAktive()) {
			  Motion_Run((motorDir_t) (((Controlword_temp) & 0x0004) >> 2));
			  fFlagDauerbetrieb=1;
			  Motion_ResetInPosition();
			}
		} else
			Stepper1.Fehler = 1;

	}else{
		if (pStepper->Enabled() and fFlagDauerbetrieb) {				//Dauebetieb beenden
			Motion_SoftStop();
			GetPosition();
			if (!Motion_GetAktive()){
			  fZielposition = fIstPosition;
			  fFlagDauerbetrieb=0;
			}
		}
	}

	// Abschalten
	if ((((Controlword_temp) & 0x0001) == 0)
			and (((Controlword_temp) & 0x0002) == 0)) {
		Motion_SoftStop();
		if (!Motion_GetAktive() and pStepper->Enabled()) {
			Motion_ResetInPosition();
			pStepper->Disable();
		}
	}

	// Calibrieren
	if (((Controlword_temp) & 0x080) != 0) {
		if (calibrateEnable == 0) {
			uint16_t StepsToMove = 1000;
			calibrateEnable = 1;

			LMSenable = 0;							//LMS auschalten um auf schritte zu fahren
			Stepper1.tmc_init(Achse0.IMAX / 1000.0);
			pStepper->Enable();
			pMesssystem->ReadPosition();
			HAL_Delay(10);
			uint32_t StartPos = pMesssystem->Position;
			Motion_SetMaxSpeed(1000);
			Motion_SetAcceleration(1000);
			Motion_Move(FORWARD, StepsToMove);

			do {
				HAL_Delay(100);
			} while (Motion_GetAktive());

			HAL_Delay(100);
			pMesssystem->ReadPosition();
			HAL_Delay(10);
			int32_t deltaPos = (StartPos - pMesssystem->Position);
			pMesssystem->IncPerStep = (abs(deltaPos) / StepsToMove) * 1000;

			pUNICAN->WriteEEPROM(EEADR_EManReg_1, pMesssystem->IncPerStep, 4);
			if (deltaPos < 0) {
				pMesssystem->andersrum = 0;				//andersrum
				Achse0.CONFIG &= ~BITMASK(ENCDIR_LR_BIT);
			} else {
				pMesssystem->andersrum = 1;				//sorum
				Achse0.CONFIG |= BITMASK(ENCDIR_LR_BIT);
			}
			pUNICAN->SetEEPROMRegleparameters(0, Achse0);
			pStepper->Disable();
			LMSenable = Achse0.CONFIG & (UI1 << LMS_EN_BIT) ? 1 : 0;
		}
	} else
		calibrateEnable = 0;

	// Input / Output
	SetPin(OE4_GPIO_Port, OE4_Pin, (Controlword_temp & 0x1000) >> 12);	// X2
	SetPin(OE3_GPIO_Port, OE3_Pin, (Controlword_temp & 0x2000) >> 13);	// X3
	SetPin(OE2_GPIO_Port, OE2_Pin, (Controlword_temp & 0x4000) >> 14);	// X6
	SetPin(OE1_GPIO_Port, OE1_Pin, (Controlword_temp & 0x8000) >> 15);	// X7

	//Enable
	SetPin(NIN4_GPIO_Port, NIN4_Pin, (~Controlword_temp & 0x100) >> 8);	// X2
	SetPin(NIN3_GPIO_Port, NIN3_Pin, (~Controlword_temp & 0x200) >> 9);	// X3
	SetPin(NIN2_GPIO_Port, NIN2_Pin, (~Controlword_temp & 0x400) >> 10);// X6
	SetPin(NIN1_GPIO_Port, NIN1_Pin, (~Controlword_temp & 0x800) >> 11);// X7
}

void SetPin(GPIO_TypeDef *GPIOx, uint16_t GPIO_Pin, uint8_t PinState) {
	if (PinState)
		HAL_GPIO_WritePin(GPIOx, GPIO_Pin, GPIO_PIN_SET);
	else
		HAL_GPIO_WritePin(GPIOx, GPIO_Pin, GPIO_PIN_RESET);
}

void Handle_Statusword(void) {
	uint16_t vStatusword;
	vStatusword = 0;

	if (pStepper->Enabled()){
		vStatusword |= BITMASK(0);		//enable
		vStatusword |= BITMASK(1);			//in Referenz immer an
	}

	if (Stepper1.Fehler > 0)
		vStatusword |= BITMASK(2);
	if (Motion_GetInPosition()) {
		vStatusword |= BITMASK(3);		//in grop Position
		vStatusword |= BITMASK(4);		//in Position
		if (Motion_GetInPosition() != InPosition) {
			Stepper1.tmc_init((Achse0.IMAX / 1000.0) / 2);// Haltestrom auf die H�lfte begrenzen
			InPosition = Motion_GetInPosition();
		}
	} else {
		if (Motion_GetInPosition() != InPosition) {
			Stepper1.tmc_init((Achse0.IMAX / 1000.0));
			InPosition = Motion_GetInPosition();
		}
	}
	if (fFlagDauerbetrieb)
		vStatusword |= BITMASK(5);
	if (VPLUS_VALUE < VPlusMin)
		vStatusword |= BITMASK(6);
	if (!HAL_GPIO_ReadPin(CFO4_GPIO_Port, CFO4_Pin))	//Status X2 anzeigen
		vStatusword |= BITMASK(8);
	if (!HAL_GPIO_ReadPin(CFO3_GPIO_Port, CFO3_Pin))	//Status X3 anzeigen
		vStatusword |= BITMASK(9);
	if (!HAL_GPIO_ReadPin(CFO2_GPIO_Port, CFO2_Pin))	//Status X6 anzeigen
		vStatusword |= BITMASK(10);
	if (!HAL_GPIO_ReadPin(CFO1_GPIO_Port, CFO1_Pin))	//Status X7 anzeigen
		vStatusword |= BITMASK(11);
	if (VPLUS_VALUE > VPlus48)
		vStatusword |= BITMASK(15);

	CAN_Statusword = vStatusword;

}

void GetPosition(void) {
	if (LMSenable) {
		pMesssystem->ReadPosition();
		int32_t read = pMesssystem->Position;

		#if (defined(Warparound_Filterslide_Pos) && (Warparound_Filterslide_Pos != 0))
		  uint8_t PosLenth = pMesssystem->getPosLenth();
		  if (read & 1<<(PosLenth-1))		//h�chstes Bit vergleichen
			  read |= (-1 <<(PosLenth)); 	//restlichen Stellen mit 1 auff�llen bis 32 bit voll sind
		#endif
		  fIstPosition = read;
	} else
		fIstPosition = Motion_GetPosition();

	fSchleppfehler = Motion_GetPositionError();
}

void writeEEPROM(uint8_t Register, uint32_t data){

	switch (Register) {
		case 1:
			pUNICAN->WriteEEPROM(EEADR_EManReg_1, data, 2);
			break;
		case 2:
			pUNICAN->WriteEEPROM(EEADR_EManReg_2, data, 2);
			break;
		case 3:
			pUNICAN->WriteEEPROM(EEADR_EManReg_3, data, 2);
			break;
		default:
			break;
	}
}

void LoadParameters(void) {
	uint8_t vLoadDefaults;
	Achse0 = pUNICAN->GetEEPROMRegleparameters(0);

	if (Achse0.LRP == 0xFFFF or Achse0.LRP == -1)
		vLoadDefaults = true;
	else
		vLoadDefaults = false;

	if (vLoadDefaults) {
		Achse0.VSOLL_LS = DEFAULT_VSOLL;
		Achse0.ASOLL_LS = DEFAULT_ASOLL;
		Achse0.IMAX = DEFAULT_IMAX;
		Achse0.IMIN = DEFAULT_IMIN;
		Achse0.KI = DEFAULT_TII;
		Achse0.KR = DEFAULT_DRP;
		Achse0.TI = DEFAULT_DRTI;
		Achse0.TD = DEFAULT_DRTD;
		Achse0.LRP = DEFAULT_LRP;
		Achse0.CONFIG = DEFAULT_CONFIG
		pUNICAN->SetEEPROMRegleparameters(0, Achse0);
		pUNICAN->WriteEEPROM(EEADR_EManReg_1, 0, 4);
	}

	if(Achse0.IMAX > MAX_I){					//Strom > 1,45A Achtung auf Temperaturen achten
		Achse0.IMAX = MAX_I;
		pUNICAN->SetEEPROMRegleparameters(0, Achse0);
	}


	LMSenable = Achse0.CONFIG & (UI1 << LMS_EN_BIT) ? 1 : 0;
	pMesssystem->andersrum = Achse0.CONFIG & (UI1 << ENCDIR_LR_BIT) ? 1 : 0;
	pMesssystem->IncPerStep = pUNICAN->ReadEEPROM(EEADR_EManReg_1, 4);

	pMesssystem-> setPosLenth(pUNICAN->ReadEEPROM(EEADR_EManReg_3, 2));
}

/* USER CODE END 4 */

/**
 * @brief  This function is executed in case of error occurrence.
 * @retval None
 */
void Error_Handler(void)
{
	/* USER CODE BEGIN Error_Handler_Debug */
	/* User can add his own implementation to report the HAL error return state */
	while (1) {
	}
	/* USER CODE END Error_Handler_Debug */
}

#ifdef USE_FULL_ASSERT

/**
 * @brief Reports the name of the source file and the source line number
 * where the assert_param error has occurred.
 * @param file: pointer to the source file name
 * @param line: assert_param error line source number
 * @retval None
 */
void assert_failed(uint8_t* file, uint32_t line)
{
	/* USER CODE BEGIN 6 */
	/* User can add his own implementation to report the file name and line number,
	 tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
	/* USER CODE END 6 */

}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
