#include <mem_queue.h>

bool tMem_queue::q(tMem_Dataset num)
{
  uint8_t result = false;
  uint32_t prim;
  prim = __get_PRIMASK();
  __disable_irq();
  if(!(tail+1==head || (tail+1>=maxQueue && !head)))
  {
    tail++;
    if(tail==maxQueue)
       tail = 0; // cycle around
    queue[tail] = num;
    result = true;
  }
  if (!prim)
    __enable_irq();
  return result;
}

bool tMem_queue::deq(tMem_Dataset *get)
{
  uint8_t result = false;
  uint32_t prim;
  prim = __get_PRIMASK();
  __disable_irq();
  if (!((head == tail) || (head>=maxQueue)))
  {
    head++;
    if(head==maxQueue)
       head = 0;
    *get = queue[head];
    result = true;
  }
  if (!prim)
    __enable_irq();
  return result;
}


