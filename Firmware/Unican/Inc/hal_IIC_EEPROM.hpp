/*
 * ServoEEPROM.hpp
 *
 *  Created on: 28.02.2017
 *      Author: Hildebrandt
 */

#ifndef HAL_IIC_EEPROM_HPP_
#define HAL_IIC_EEPROM_HPP_


  #if defined(STM32F767xx)
  #include "stm32f7xx.h"
  #include "stm32f7xx_hal.h"
  #elif defined(STM32F40_41xxx)
  #include <stm32f4xx.h>
  #include <stm32f4xx_hal.h>
  #elif defined(STM32F415xx)
  //#include <stm32f415xx.h>
  #include <stm32f4xx.h>
  #include "stm32f4xx_hal.h"
  #elif defined(STM32F10X_MD) || defined(STM32F103xB)
  #include <stm32f1xx.h>
  #include <stm32f1xx_hal.h>
  #endif



  #define EEPROM_TYPE                M2402        // 2 Kbit

  #define I2C_TIMEOUT_MAX 500
  #define MEM_DEVICE_WRITE_ADDR 0xA0
  #define MEM_DEVICE_READ_ADDR 0xA1

  #if (EEPROM_TYPE == M2402)
    #define EEpromMirror_Size    256
    #define _MemAddSize I2C_MEMADD_SIZE_8BIT
  #elif (EEPROM_TYPE == M2404)
    #define EEpromMirror_Size    512
  #elif (EEPROM_TYPE == M2408)
    #define EEpromMirror_Size    1024
  #endif

  #define EEPROM_IIC_TIMEOUT_MS  20

  #define EManReg_Area_size   10

  typedef enum {M2401,M2402,M2404,M2408,M2416} eepromtype;

  enum tEEPROM_STATE {EEPROM_WS_IDLE, EEPROM_WS_DATA};

  typedef struct
  {
    uint16_t addr;
    uint8_t data;
  } tEEPROM_DATA;


  //**** EEPROM Konstanten
  #define EEPROM_REGISTER_ADDR      (uint32_t) 0x0d00
  #define EEPROM_BASE               (uint32_t) 0x0000
  #define EEPROM_SIZE               (uint32_t) 0x0100
  #define EEADR_EEData_OK           (EEPROM_BASE + 1)
  #define EEADR_CanOpen_Node_ID     (EEPROM_BASE + 3)
  #define EEADR_Serial_Number       (EEPROM_BASE + 4)
  #define EEADR_SERVO_0_Parameter_B (EEPROM_BASE + 8)
  #define EEADR_SERVO_1_Parameter_B (EEPROM_BASE + 28)
  #define EEADR_SERVO_2_Parameter_B (EEPROM_BASE + 48)
  #define EEADR_SERVO_3_Parameter_B (EEPROM_BASE + 68)
  #define EEADR_EManReg_Area        (EEPROM_BASE + 88)
  #define EEADR_EManReg_1           (EEADR_EManReg_Area + 0)
  #define EEADR_EManReg_2           (EEADR_EManReg_Area + 2)
  #define EEADR_EManReg_3           (EEADR_EManReg_Area + 4)
  #define EEADR_EManReg_4           (EEADR_EManReg_Area + 6)
  #define EEADR_EManReg_5           (EEADR_EManReg_Area + 8)
  #define EEADR_EManReg_6           (EEADR_EManReg_Area + 10)
  #define EEADR_EManReg_7           (EEADR_EManReg_Area + 12)
  #define EEADR_EManReg_8           (EEADR_EManReg_Area + 14)
  #define EEADR_EManReg_9           (EEADR_EManReg_Area + 16)
  #define EEADR_EManReg_10          (EEADR_EManReg_Area + 18)
  #define EEADR_Betriebsstunden     (EEPROM_BASE + 108)
  #define EEADR_Birthday            (EEPROM_BASE + 110)
  #define EEADR_Next_free           (EEPROM_BASE + 112)

  #define Error_Betriebsstunden_Size                 11
  #define EEADR_Errors                (EEPROM_BASE + 208)
  #define EEADR_Error_Betriebsstunden (EEPROM_BASE + 230)

#define  MAX_HANDLERS 10


class tServoEEPROM
{
  private:
  uint8_t* pEEpromReadMirror;
  uint8_t* pEEpromWriteMirror;
  uint32_t fEEpromMirrorSize;
  uint8_t  fSerialEEPROMConnect;
  volatile uint16_t EEPROM_Timer;
  tEEPROM_DATA fEPPROM_DATA_LATCH;
  uint8_t EEPROM_STATE;

  void Set_EEPROMMIRRROR(uint16_t Addr, uint8_t value);
  uint8_t Get_EEPROMMIRROR_WIRTE(uint16_t Addr);
  uint8_t Check_EEPROMMIRRROR(uint16_t *Addr);


 public:

  I2C_HandleTypeDef*  pMyhI2C;
  tServoEEPROM(int Size);
   ~tServoEEPROM(void);

  static tServoEEPROM* handlers[MAX_HANDLERS];
  static uint16_t fInstanceCounter;
  uint16_t fMyInstanceIndex;

  uint8_t ConnectSerialEEPROM(I2C_HandleTypeDef* hI2C);
  void ConnectFlashEEPROM(uint32_t* Adress);

  uint8_t Get_EEPROM(uint16_t Addr);
  void Set_EEPROM(uint16_t Addr, uint8_t value);
  void EEStoreValue(uint16_t Addr, uint32_t Value, uint8_t count);
  uint32_t EEReadValue(uint16_t Addr, uint8_t count);


  uint8_t* GetMirrorAdr(void);

  uint8_t EEPROM_WRITER_DONE(void);
  void    EEPROM_TIMER_CALL_EVERY_MS(void);
  void   EEPROM_WRITER(void);
  void EEPROM_Done(void);
  void EEPROMM_Error(void);
  uint32_t MirrorSize(void);

};



#endif /* UTILS_INC_SERVOEEPROM_HPP_ */
