/**
  ******************************************************************************
  * File Name          : unican.cpp
  * Description        : This file contains the common defines of the application
  ******************************************************************************
  *
  ******************************************************************************
  **/
#include <hal_unican.h>
#include <main.h>
#include <string.h>

#define RAM_BOOTLOADER  1

#if (!RAM_BOOTLOADER)
  #if defined(STM32F10X_MD) || defined(STM32F103xB)
  #define BOOTLOADER_BASE  (uint32_t) FLASH_BASE+0x1E400
  #elif defined(STM32F767xx)
  #define BOOTLOADER_BASE  (uint32_t) FLASH_BASE+0x180000
  #else
  #define BOOTLOADER_BASE  (uint32_t) FLASH_BASE+0x8C000
  #endif
#else
  #if defined(STM32F10X_MD) || defined(STM32F103xB)
  #include <flashtoolF103.h>
  #elif defined(STM32F767xx)

  #else
  #include <flashtoolF415.h>
  #endif
#endif

#define VariablenRegistersCount 10
tCANopenRegisterLUT VariablenRegisters[VariablenRegistersCount];
const tCANopenRegisterLUT DEF_CANopenRegisterLUT = {0,0,1,0,4};

//tSERVO_EEPROM_Default *Achse0 = ( tSERVO_EEPROM_Default*) EEADR_SERVO_0_Parameter_B;
//tSERVO_EEPROM_Default *Achse1 = ( tSERVO_EEPROM_Default*) EEADR_SERVO_1_Parameter_B;
//tSERVO_EEPROM_Default *Achse2 = ( tSERVO_EEPROM_Default*) EEADR_SERVO_2_Parameter_B;
//tSERVO_EEPROM_Default *Achse3 = ( tSERVO_EEPROM_Default*) EEADR_SERVO_3_Parameter_B;

tEEPROM_Registers *EManReg = ( tEEPROM_Registers*) EEADR_EManReg_Area;
tSERVO_EEPROM_Registers *SERVO_0 = ( tSERVO_EEPROM_Registers*) EEADR_SERVO_0_Parameter_B;
tSERVO_EEPROM_Registers *SERVO_1 = ( tSERVO_EEPROM_Registers*) EEADR_SERVO_1_Parameter_B;
tSERVO_EEPROM_Registers *SERVO_2 = ( tSERVO_EEPROM_Registers*) EEADR_SERVO_2_Parameter_B;
tSERVO_EEPROM_Registers *SERVO_3 = ( tSERVO_EEPROM_Registers*) EEADR_SERVO_3_Parameter_B;

//tUnican* handlers[2];

uint16_t volatile* SERVO_Registers[10] =  // Ohne Offset
{
  (uint16_t volatile*)(0x24),  // 0 S2_VSOLL_LS   (Test 0x824 f�r 0A24)
  (uint16_t volatile*)(0x26),  // 1 S2_ASOLL_LS    _S2_LP(0x26)
  (uint16_t volatile*)(0x28),  // 2 S2_IMAX        _S2_LP(0x28)
  (uint16_t volatile*)(0x38),  // 3 S2_IMIN        _S2_LP(0x38)
  (uint16_t volatile*)(0x0A),  // 4 S2_KI          _S2_P(0x0B)
  (uint16_t volatile*)(0x0C),  // 5 S2_KR          _S2_P(0x0D)
  (uint16_t volatile*)(0x1C),  // 6 S2_TI          _S2_LP(0x1C)
  (uint16_t volatile*)(0x2C),  // 7 S2_TD          _S2_LP(0x2C)
  (uint16_t volatile*)(0x3C),  // 8 S2_LRP         _S2_P(0x3D)
  (uint16_t volatile*)(0x0E)   // 9 S2_CONFIG      _S2_LP(0x0E)
};
#define ConstantenRegistersCount 11
const tCANopenRegisterLUT ConstantenRegisters[ConstantenRegistersCount] = {
  { FlashOperation, 0x00, 0, 5 ,1},                                                               // 1
  { FlashOperation, FlashStartAdress, 0, FLASH_BASE, sizeof((uint32_t *)FLASH_BASE) },            // 2
  { FlashOperation, FlashSize, 0, STM32_FLASH_SIZE, sizeof((uint32_t *)STM32_FLASH_SIZE) },       // 3
  { FlashOperation, FlashPageSize, 0, STM32_PAGE_SIZE, sizeof((uint32_t *)STM32_PAGE_SIZE) },     // 4
  { ExtFlashOperation, 0x00, 0, 5 ,1},
  { ExtFlashOperation, FlashStartAdress, 0, 0, 4},
  { ExtFlashOperation, FlashSize, 0, 0, 4},
  { ExtFlashOperation, FlashPageSize, 0, 0, 4},
  { MemoryOrganisation, 0, 0, 20,  1},
  { MemoryOrganisation, 1, 0, FLASH_BASE , 4},
  { MemoryOrganisation, 2, 0, STM32_FLASH_SIZE, 4} /*,
  { MemoryOrganisation, 3, 0, CCMDATARAM_BASE, 4},
  { MemoryOrganisation, 4, 0, 0x10000, 4},   // (64 KB)
  { MemoryOrganisation, 5, 0, SRAM1_BASE, 4},
  { MemoryOrganisation, 6, 0, 0x20000, 4},   // (112 KB)fdgdfgsdfgdfgdfg
  { MemoryOrganisation, 7, 0, SRAM2_BASE, 4},
  { MemoryOrganisation, 8, 0, 0x4000, 4},   // (16 KB)
  { MemoryOrganisation, 9, 0, SRAM3_BASE, 4},
  { MemoryOrganisation, 10, 0, 0x10000, 4},   // (64 KB)
  { MemoryOrganisation, 11, 0, APB1PERIPH_BASE, 4},
  { MemoryOrganisation, 12, 0, 0x10000, 4},   // (64 KB)
  { MemoryOrganisation, 11, 0, APB2PERIPH_BASE, 4},
  { MemoryOrganisation, 12, 0, 0x10000, 4},   // (64 KB)    32KB CODE SIZE Begrenzung
  { MemoryOrganisation, 11, 0, AHB1PERIPH_BASE, 4},
  { MemoryOrganisation, 12, 0, 0x10000, 4},   // (64 KB)
  { MemoryOrganisation, 11, 0, AHB2PERIPH_BASE, 4},
  { MemoryOrganisation, 12, 0, 0x10000, 4},  // (64 KB)
  { MemoryOrganisation, 13, 0, BKPSRAM_BASE, 4},
  { MemoryOrganisation, 14, 0, 0x1000, 4},   // (4 KB)
  { MemoryOrganisation, 19, 0, FSMC_R_BASE, 4},
  { MemoryOrganisation, 20, 0, 0x1000, 4}   */
};

#define SC_Flash_Enabled        (uint32_t) 0x01
#define SC_NMT_STOPPED          (uint32_t) 0x02
#define SC_Bootloader_Enabled   (uint32_t) 0x04
#define SC_Toggle_Bit           (uint32_t) 0x10
#define SC_NODEGUARD_Toggle_Bit (uint32_t) 0x20
#define SC_Segmented_Upload     (uint32_t) 0x40
#define SC_Segmented_Download   (uint32_t) 0x80

void(*SysMemBootJump) (void);

#define CAN_SOFT_FIFO 1

#if  CAN_SOFT_FIFO == 1
  tCAN_FIFO           TXC_FIFO;
#else
  tCAN_FIFO_STATISTIK RXC_FIFO, TXC_FIFO;
#endif

//extern uint8_t FPGA_Mirror[];

static   CAN_TxHeaderTypeDef   TxHeader;
static   CAN_RxHeaderTypeDef   RxHeader;
static   uint8_t               TxData[8];
static   uint8_t               RxData[8];
static   uint32_t              TxMailbox;
static   uint32_t              CAN_IER_BUFFER;

uint32_t Debug;
static  tUnican* Master_handlers[MAX_HANDLERS] = {0};
static  uint16_t  fMaster_InstanceCounter = 0;

#ifndef UNICAN_MASTER_AND_SLAVE
void HAL_CAN_RxFifo0MsgPendingCallback(CAN_HandleTypeDef *hcan)
#else
void CANSLAVE_RxCpltCallback(CAN_HandleTypeDef* hcan)
#endif
{
  uint8_t i;
  uint8_t  for_me = false;
  for (i=0; i<fMaster_InstanceCounter; i++)
    if (Master_handlers[i]->fhcan == hcan)
      for_me = true;
  
  if (for_me && HAL_CAN_GetRxMessage(hcan, CAN_RX_FIFO0, &RxHeader, RxData) == HAL_OK)
  {
    for (i=0; i<fMaster_InstanceCounter; i++)
    {
      if (Master_handlers[i]->fhcan == hcan)
      {
        Master_handlers[i]->CAN_Receiver(&RxHeader, RxData);
      }
    }
  }
}

void HAL_CAN_ErrorCallback(CAN_HandleTypeDef *hcan)
{
  uint8_t i;
  for (i=0; i<fMaster_InstanceCounter; i++)
  {
    if (Master_handlers[i]->fhcan == hcan)
    {
      Master_handlers[i]->CAN_Error();
    }
  }
}


#ifdef __cplusplus
 extern "C" {
#endif

#if (!defined(STM32F767xx))
#if (RAM_BOOTLOADER)
void FlashToolLoader(void)
{
  void(*FlashLoaderJump) (void);

  BeforeFlashloader();
  SysTick->CTRL = 0;
  SysTick->LOAD = 0;
  SysTick->VAL = 0;
  RCC->CIR = 0x00000000;  // Disable Interrups
  // HAL_RCC_DeInit();      // RCC->CIR = 0x00000000;  // Disable Interrups
  __set_PRIMASK(1);  // Disable Interrups

#if defined(STM32F10X_MD) || defined(STM32F103xB)
  memcpy((uint32_t *) (SRAM_BASE), flashtool, sizeof(flashtool));
  uint32_t SP   = *((uint32_t *) (SRAM_BASE));
  __set_MSP(SP);
  FlashLoaderJump=(void (*)(void)) (*((uint32_t*) (SRAM_BASE+4)));

#elif defined(STM32F40_41xxx) || defined(STM32F415xx)
  memcpy((uint32_t *) (SRAM1_BASE), flashtool, sizeof(flashtool));
  uint32_t SP   = *((uint32_t *) (SRAM1_BASE));
  __set_MSP(SP);
  FlashLoaderJump=(void (*)(void)) (*((uint32_t*) (SRAM1_BASE+4)));
#endif
  FlashLoaderJump();
  while(1);
}

#else

void CiaBootLoader(uint32_t BootloaderStatus)
{
	uint32_t SP   = *((uint32_t *) (BOOTLOADER_BASE));
  SysMemBootJump=(void (*)(void)) (*((uint32_t*) (BOOTLOADER_BASE+4)));
  if (BootloaderStatus == 1)
  {
     RCC->CIR = 0x00000000;  // Disable Interrups
     __set_PRIMASK(1);       // Disable Interrups
     __set_MSP(SP);          // Defaultwert
     SysMemBootJump();
     while(1);
  }
}
#endif
#endif // (!defined(STM32F767xx))

uint32_t Intervall(uint32_t min, uint32_t Value, uint32_t max)
{
  if (min > Value) return min;
  if (max < Value) return max;
  return Value;
}

#ifdef __cplusplus
}
#endif

#if defined(STM32F10X_MD) || defined(STM32F103xB)
#else
 void BootLoaderInit(uint32_t BootloaderStatus)
 {
   uint32_t SP   = *((uint32_t *) 0x1fff0000);               // Wert aus dem ROM
   SysMemBootJump=(void (*)(void)) (*((uint32_t*) 0x1fff0004));   // Zeiger aus dem ROM
   if (BootloaderStatus == 1)
   {
      //RCC_DeInit();
      HAL_RCC_DeInit();
      SysTick->CTRL = 0;
      SysTick->LOAD = 0;
      SysTick->VAL = 0;

      __set_PRIMASK(1);          // Disable Interrups
      __set_MSP(SP);             // Wert aus dem ROM
      //__set_MSP(0x20001000);  // Defaultwert

      SysMemBootJump();

      while(1);
   }
 }
#endif
 __weak uint8_t CUSTOM_Register_Write(uint16_t Register, uint8_t Sub, uint32_t *Data, uint32_t *Size)
 {
   return 0;
 }

 __weak uint8_t CUSTOM_Register_Read(uint16_t Register, uint8_t Sub, uint32_t *Data, uint32_t *Size)
 {
   return 0;
 }

 __weak int16_t SPI_CANopen_Receive16(uint8_t Adress)
 {
   return 0;
 }
 __weak void SPI_CANopen_Send16(uint8_t Adress, uint16_t Data)  // Register folowing space 0x200 - 0x27F
 {

 }

 __weak void OnCUSTOM_Register_Read_DONE(void)
 {

 }

 __weak void OnCUSTOM_Register_Write_DONE(void)
 {

 }

 __weak void BeforeFlashloader(void)
 {
   // Stoppe All DMA Funktionen die in der Main laufen!

 }

uint32_t tUnican::State_Control(void)
{
  return fState_Control;
}

void tUnican::Reset_Data(void)
{
  uint8_t i,j;
  fEnabled          = 0;
  Kaltstart_request = 0;

  for (i=0;i<4;i++)
  {
    for (j=0;j<8;j++)
    {
      RPDO[i][j]               = &fNUL;
      RPDO_remember_mask[i][j] = 0x00;
      fTPDO_DATA_OLD[i][j]     = 0x00;
    }
    RPDO_Data_New[i] = &fNUL;
    fTPDO_enabled[i]  = false;
    fSend_sync_PDO[i] = false;
    fTPDO_enabled_old[i] = false;
    fSoftware_position_limit[i << 1]     = 0x80000000;
    fSoftware_position_limit[1+(i << 1)] = 0x7FFFFFFF;
  }

  fEmergency_Error_code     = No_Error;
  fEmergency_Error_code_old = Generic_Error;
  fError_register           = 0;
  fError_register_old       = Generic; // Senden Ausl�sen
  fStatus_register          = 0x00000000;
  fStatus_register_old      = 0x00000001;
  fControlword              = 0x0000;
  fStatusword               = 0x0000;
  fDevice_select            = true;
  fHeartbeatTime            = 0;                 // default Disable
  fState_Control            = SC_Preoperational; // Auf fState_Control nur im Interrupt schreibend zugreifen !
  fCycleMax                 = 0;
  fCycleMin                 = 0xFFFFFFFF;
  fRX_Errors                = 0;
  fTX_Errors                = 0;
  fBytes_to_Send            = 0;
  fPointer_for_Send         = &fNUL;


   for (i=0; i< VariablenRegistersCount; i++)
      VariablenRegisters[i] = DEF_CANopenRegisterLUT;

   VariablenRegisters[0].CANopenReg = 0x1000;
   VariablenRegisters[0].IsPointer  = 0;
   VariablenRegisters[0].Value      = 0;     // Device TYPE

   VariablenRegisters[1].CANopenReg = 0x1001;
   VariablenRegisters[1].Length     = 2;
   VariablenRegisters[1].Value      = (uint32_t) &fError_register;

   VariablenRegisters[2].CANopenReg = 0x1002;
   VariablenRegisters[2].Length     = 2;
   VariablenRegisters[2].Value      = (uint32_t) &fStatus_register;

   VariablenRegisters[3].CANopenReg = 0x1008;
   VariablenRegisters[3].Value      = (uint32_t) fHW_NAME;

   VariablenRegisters[4].CANopenReg = 0x1009;
   VariablenRegisters[4].Value      = (uint32_t) fHW_VER;

   VariablenRegisters[5].CANopenReg = 0x1018;
   VariablenRegisters[5].IsPointer  = 0;
   VariablenRegisters[5].Value      = 4;

   VariablenRegisters[6].CANopenReg = 0x1018;
   VariablenRegisters[6].SubIndex   = 4;
   VariablenRegisters[6].Value      = (uint32_t) &fSerial_Number;

   VariablenRegisters[7].CANopenReg = 0x6040;
   VariablenRegisters[7].Value      = (uint32_t) &fControlword;

   VariablenRegisters[8].CANopenReg = 0x6041;
   VariablenRegisters[8].Value      = (uint32_t) &fStatusword;

   VariablenRegisters[9].CANopenReg = 0x5482;
   VariablenRegisters[9].Value      = (uint32_t) &uwTick;


   /*ToDo: war drin
   Reset_Communication = true;*/

 }

 uint16_t tUnican::BootloaderVersion(void)
 {
   #if (!RAM_BOOTLOADER)
     uint32_t  ptr, sum = 0;
     uint16_t result = 0;
     #if defined(STM32F10X_MD) || defined(STM32F103xB)
     for (ptr = BOOTLOADER_BASE;ptr <= (FLASH_BANK1_END);ptr++)
       sum = sum + *(uint8_t *) ptr;
     switch (sum)
      {
        case 677167  : result = 0x0104; break;
        case 702739  : result = 0x0103; break;
        case 1827840 : result = 0x0000; break; // alles FF
        case 1827585 : result = 0x0000; break; // fast alles FF
        default      : result = 0x0001; break; // unbekannter Inhalt
      }

     return (result);  // nur 128kb Flash
     #else
     for (ptr = BOOTLOADER_BASE;ptr < (BOOTLOADER_BASE+0x2000);ptr++)
       sum = sum + *(uint8_t *) ptr;
     switch (sum)
     {
       case 753825  : result = 0x0100; break;
       case 743838  : result = 0x0101; break;
       case 754782  : result = 0x0102; break;
       case 754527  : result = 0x0102; break; // Manchmal fehlt ein ff  bei 0x0808DFB8 durch STMCanflash 1.4
       case 771883  : result = 0x0103; break;
       case 771628  : result = 0x0103; break;
       case 1475643 : result = 0x0105; break;
       case 1475388 : result = 0x0105; break;
       case 2088960 : result = 0x0000; break; // alles FF
       case 2088705 : result = 0x0000; break; // fast alles FF
       default      : result = 0x0001; break; // unbekannter Inhalt
     }
     return (result);
     #endif
   #else
     #if (!defined(STM32F767xx))
       return (flashtoolversion);
     #else
       return (0x0000);
     #endif
   #endif
 }

 uint8_t tUnican::Canopen_Register_Read(uint32_t *Registerset, uint32_t size, uint16_t Register, uint8_t Sub, uint32_t *Data, uint32_t *Size)
 {
   uint8_t i, len;
   uint32_t SetSize ;
   SetSize = size / sizeof(tCANopenRegisterLUT);


   for (i=0;i<SetSize;i++)
   {
     if ((Register == ((tCANopenRegisterLUT*)Registerset)[i].CANopenReg) && (Sub == ((tCANopenRegisterLUT*)Registerset)[i].SubIndex))
     {
       if (((tCANopenRegisterLUT*)Registerset)[i].IsPointer)
         *Data = * ((uint32_t*) (((tCANopenRegisterLUT*)Registerset)[i].Value));
       else
         *Data = ((tCANopenRegisterLUT*)Registerset)[i].Value;
       len = ((tCANopenRegisterLUT*)Registerset)[i].Length;
       if (len == 1) *Data &= 0xFF;
       if (len == 2) *Data &= 0xFFFF;
       if (len == 3) *Data &= 0xFFFFFF;
       return 1;
     }
   }
   return 0;
 }


 void tUnican::Send_NODEGUARD(uint8_t Toggle, uint8_t Status)
 {
	struct tcan_buf tempBuf;
	tempBuf.id_field = fNode_ID + FUNCTION_CODE_LIVEGUARD;
	tempBuf.dlc = 1;
    if(Toggle)
	  tempBuf.data_field[0]= (uint8_t) 0x80 | Status;
    else
	  tempBuf.data_field[0]= (uint8_t) 0x7F & Status;

    CAN_FIFO_ADD(&TXC_FIFO, &tempBuf, 0);
 }

 void tUnican::Send_trans_IRQ1(uint8_t aData[], uint8_t scs, uint32_t data)
 {
   struct tcan_buf tempBuf;
   tempBuf.id_field = fNode_ID + FUNCTION_CODE_SDO_TX;
   tempBuf.dlc = 8;

   tempBuf.data_field[0]     = scs;
   tempBuf.data_field[1]     = aData[1];
   tempBuf.data_field[2]     = aData[2];
   tempBuf.data_field[3]     = aData[3];
   tempBuf.data_field[4]     = data;
   tempBuf.data_field[5]     = data>>8;
   tempBuf.data_field[6]     = data>>16;
   tempBuf.data_field[7]     = data>>24;

   CAN_FIFO_ADD(&TXC_FIFO, &tempBuf, 0);
 }

 void tUnican::Send_trans_IRQ2(uint8_t scs, uint8_t* Adresse)
 {
   uint8_t   i ;
   struct tcan_buf tempBuf;
   uint16_t  Size = 7-((scs >> 1) & 0x07);
   // Die SPI Variante biegt den Register folowing Space auf sie SPI Schnittstelle um

   tempBuf.id_field = FUNCTION_CODE_SDO_TX | fNode_ID;
   tempBuf.dlc = 8;
   tempBuf.data_field[0] = scs;
   for (i=0;i<7;i++)
   {
	 if (i >= Size)
	   tempBuf.data_field[i+1] = 0;
	 else
	   tempBuf.data_field[i+1] = *(Adresse + i);
   }


    if (!CAN_FIFO_ADD(&TXC_FIFO, &tempBuf, 0))
    {
      fError_register           |= Communication;
      fEmergency_Error_code      = CAN_Overrun + 3;
    }

 }

void tUnican::Send_Register_unknown( uint8_t aData[])
 {
   Send_trans_IRQ1( aData , ABORT_DOMAIN_TRANSFER , 0x060200013);
 }

void tUnican::Send_Segmented_Upload_Responce( uint8_t aData[], uint32_t Adresse, uint32_t Size)
{
  Send_trans_IRQ1( aData, INITIATE_UPLOAD_RESPONCE |  SDO_xCS_SIZE_DEFINED_IN_DATA , Size);
  fBytes_to_Send    = Size;
  fState_Control &= ~SC_Toggle_Bit;
  fState_Control |= SC_Segmented_Upload;
  fPointer_for_Send = (uint8_t *)Adresse;
}

void tUnican::Send_Segmented_Download_Responce( uint8_t aData[], uint32_t Adresse, uint32_t Size)
{
  Send_trans_IRQ1( aData, INITIATE_DOWNLOAD_RESPONCE, Size);
  fBytes_to_Send    = Size;
  fState_Control &= ~SC_Toggle_Bit;
  fState_Control |= SC_Segmented_Download;
  fPointer_for_Send = (uint8_t *)Adresse;
}

/*----------------------------------------------------------------------------
  CAN NMT receive
 *----------------------------------------------------------------------------*/
void tUnican::NMT_Receive(CAN_RxHeaderTypeDef *pHeader, uint8_t aData[]){
  uint8_t i;

  //if ((aData[0] == 0xAD)  && (Message.DLC == 2))
     //CAN_Reset();  // LP Tot !!!
  if ((aData[1] == fNode_ID) && (pHeader->DLC == 2))
  {
    switch (aData[0])
    {
      case 0x82 : Reset_Communication = true; break;                // Reset Comunication
      case 0x80 : for (i=0;i<4;i++)                                // Pre Operal
                    fTPDO_enabled[i]   = fTPDO_enabled_old[i] = false;
                  fState_Control &= ~SC_NMT_STOPPED;
                  fState_Control |= SC_Preoperational;
                  break;
      case 0x81 : Kaltstart_request = true; break;   // Reset
      case 0x01 : for (i=0;i<4;i++)                                 // Operal
                  {
                    fTPDO_enabled[i]    = true;
                    fSend_sync_PDO[i] = false;
                  }
                  if ((fSerial_Number & 0xFF0000) == 0)
                  {
                    //STM32F1
                    //Zufall  = STM32_UUID[0];
                    //Zufall  = Zufall ^ STM32_UUID[1];
                    //Zufall  = Zufall ^ STM32_UUID[2];
                    uint32_t Zufall  = Unique_ID1 ^ Unique_ID2 ^ Unique_ID3;
                    Zufall ^= (Zufall >> 8);
                    Zufall &= 0x00FFFFFF;
                    Zufall |= 0x07000000;
                    fSerial_Number = Zufall;
                    WriteEEPROM(EEADR_Serial_Number, Zufall, 4);
                  }
                  fState_Control &= ~SC_NMT_STOPPED;
                  fState_Control &= ~SC_Preoperational;
                  break;
      case 0x02 : for (i=0;i<4;i++)                                 // Stopped
                    fTPDO_enabled[i] = fTPDO_enabled_old[i] = false;
                  fState_Control |= SC_NMT_STOPPED;
                  fState_Control &= ~SC_Preoperational;
                  break;
    }
  }
}


/*----------------------------------------------------------------------------
  CAN SYNC receive
 *----------------------------------------------------------------------------*/
void tUnican::SYNC_Receive(CAN_RxHeaderTypeDef *pHeader, uint8_t aData[]){
  uint8_t i;
    for (i=0;i<4;i++)
      fSend_sync_PDO[i] = true;
}
/*----------------------------------------------------------------------------
  CAN TIME receive
 *----------------------------------------------------------------------------*/
void tUnican::TIME_Receive(CAN_RxHeaderTypeDef *pHeader, uint8_t aData[]){
  tTIME  TIME_OF_DAY;
  TIME_OF_DAY.miliseconds = (aData[3] << 24) + (aData[2] << 16) + (aData[1] << 8) + aData[0];
  TIME_OF_DAY.days        = (aData[5] << 8) + aData[4];
  TIME_OF_DAY.miliseconds = TIME_OF_DAY.miliseconds & (unsigned long) 0x0FFFFFFF;

  if (fBirthday == 0xFFFF)
  {
    WriteEEPROM(EEADR_Birthday , TIME_OF_DAY.days, 2);
    fBirthday =  ReadEEPROM(EEADR_Birthday, 2);
  }
  fBirthday = TIME_OF_DAY.days;
}

/*----------------------------------------------------------------------------
  CAN LIVEGUARD receive
 *----------------------------------------------------------------------------*/
void tUnican::LIVEGUARD_Receive(CAN_RxHeaderTypeDef *pHeader, uint8_t aData[]){
  if (!fHeartbeatTime && pHeader->RTR) //  RTR = true; else RTR = false;
  {
    if (fState_Control & SC_NMT_STOPPED)
      Send_NODEGUARD(fState_Control & SC_NODEGUARD_Toggle_Bit, 4);   // Stopped
    else
      if (fState_Control & SC_Preoperational)
    	Send_NODEGUARD(fState_Control & SC_NODEGUARD_Toggle_Bit, 127); // Pre Operal
      else
    	Send_NODEGUARD(fState_Control & SC_NODEGUARD_Toggle_Bit, 5);   // Operal

    if (fState_Control & SC_NODEGUARD_Toggle_Bit)
      fState_Control &= ~SC_NODEGUARD_Toggle_Bit;
    else
      fState_Control |=  SC_NODEGUARD_Toggle_Bit;
  }
}

void tUnican::LSS_Receive(CAN_RxHeaderTypeDef *pHeader, uint8_t aData[])
{
  // Layer-Setting-Service
  // CANopen den Layer-Setting-Service (LSS) in CiA DSP-305. Mit
  //Hilfe des Layer Setting Service (LSS) kann ein LSS-Master die
  //Baudrate und Knotennummer eines LSS-Slaves �ber den CAN-Bus
  //�ndern.
  // LSS Slave adress
  switch (aData[0])
  {
    case 0x04: // Switch Mode Global
       switch (aData[1])
       {
         case 0 : //Operationsmodus einschalten
         case 1 : //Konfigurationsmodus einschalten
         default :;
       }
       break;
    case 0x13:
       if (aData[1] == 0) // Baudratentabelle, die nach CiA DSP-305
       switch (aData[2])
       {
         case 0 : // 1000kBit
         case 1 : // 800kBit
         case 2 : // 500kBit
         case 3 : // 250kBit
         case 4 : // 125kBit
         case 5 : // 100kBit
         case 6 : // 50kBit
         case 7 : // 20kBit
         case 8 : // 10kBit Send message (0x13, 0 ,0 , x , x, x, x, x) //  erfolgreich ausgef�hrt
         default : ; // Send message (0x13, 1 ,0 , x , x, x, x, x) // Baudrate wird nicht unterst�tzt
       }
       break;
    case 0x15:  // relative Zeit bis zum Einschalten der neuen Baudrate in ms
         // Delay = (aData[2] << 2) + aData[1];
       break;
    case 0x11:
      //WriteEEPROM((uint32_t) EEADR_CanOpen_Node_ID,(aData[1]) & 0x7F,1);
      //Send_trans_IRQ1( aData, 0x11 , 8);

      // Send message (0x11, 0 ,0 , x , x, x, x, x) //  erfolgreich ausgef�hrt
      //Kaltstart_request = true;
       break;

  }
}

/*----------------------------------------------------------------------------
  CAN RXPDO receive
 *----------------------------------------------------------------------------*/
void tUnican::RXPDO_Receive(uint8_t PDO, CAN_RxHeaderTypeDef *pHeader, uint8_t aData[]){
  uint8_t i;
  for (i=0;i<pHeader->DLC;i++)
    *RPDO[PDO][i] = (*RPDO[PDO][i] & RPDO_remember_mask[PDO][i]) | aData[i];
  *RPDO_Data_New[PDO] = 0xFF;
}

/*----------------------------------------------------------------------------
  CAN TXPDO receive
 *----------------------------------------------------------------------------*/

 void tUnican::TXPDO_Receive(uint8_t PDO, CAN_RxHeaderTypeDef *pHeader, uint8_t aData[])
{
  fTPDO_enabled_old[PDO] = !pHeader->RTR;  // RTR = type
  // Andernfalls ist hier eine Komponente die auf der selben ID sendet
}

/*----------------------------------------------------------------------------
  CAN RXSDO receive
 *----------------------------------------------------------------------------*/

void tUnican::RXSDO_Receive(CAN_RxHeaderTypeDef *pHeader, uint8_t aData[]){
  uint16_t Register;
  uint8_t  SubIndex;
  uint32_t Data_32;
  uint16_t Data_16;
  uint16_t Temp = 0;
  //uint16_t Status = 0;
  uint8_t  ccs, scs ,i;
  uint32_t Size;
  uint8_t  RegKnown, StandardAnswer, StartSegmented;
  uint8_t  TxIRQData[8];
  static uint16_t vCurrent_SDO_Register;

  ccs      = aData[0];
  Size     = 4 - ((ccs >> 2) & 3);
  Register = (aData[2] << 8) + aData[1];
  SubIndex = aData[3];
  Data_16  = (aData[5] << 8) + aData[4];
  Data_32  = (aData[7] << 24) + (aData[6] << 16) + Data_16;
  for(i=0;i<8;i++)
    TxIRQData[i] = aData[i];

  switch(ccs & CONTROL_BYTE_CCS_MASK)
  {
    case INITIATE_DOWNLOAD_REQUEST  : /* initiate download request */
    {
      RegKnown       = false;
      StandardAnswer = true;
      StartSegmented = true;
      if ((ccs & SDO_xCS_SIZE_CONTROL_MASK) == SDO_xCS_SIZE_DEFINED_IN_DATA)
        Size = Data_32;
      // else if ((ccs & SDO_xCS_SIZE_CONTROL_MASK) == SDO_xCS_SIZE_UNDEFINED)
      //  Size = 0;
      else // Unsegmentet
      {
        if ((ccs & SDO_xCS_SIZE_CONTROL_MASK) == SDO_xCS_SIZE_UNDEFINED)
          Size = 0;
        StartSegmented = false;
        switch ((uint16_t)Register)
        {
          case 0x1003 : // Reset Errors
            if ((SubIndex == 0x00) && (TxIRQData[4] == 0x00))
            {
              WriteEEPROM(EEADR_Errors , 0, 2);
              RegKnown = true;
            }
          break;
          case 0x1011 : // Reset Parameters
            if ((SubIndex == 0x01) && (TxIRQData[4] == 0x01))
            {
              WriteEEPROM((uint32_t) (EEADR_EEData_OK)    , 0xFF, 1);
              RegKnown          = true;
              Kaltstart_request = true;
            }
          break;
          case 0x1017 : // HeartbeatTime
            if (SubIndex == 0x00)
            {
              fHeartbeatTime  = Data_16;
              RegKnown = true;
            }
          break;
          case 0x6040 : // controlword
            if (SubIndex == 0x00)
            {
              fControlword = Data_16;
              RegKnown = true;
            }
          break;
          case 0x607D: // ARRAY software_position_limit Integer32 rw O
          {
            i = SubIndex - 1;
            if ((i & 0x07) == i)
            {
               fSoftware_position_limit[i] = Data_32;
               RegKnown = true;
            }
          }
          break;
          case Set_Serial_number : // Manufacturer Specific Object
            if (SubIndex == 0x00)
            {
              WriteEEPROM(EEADR_Serial_Number, Data_32, 4);
              RegKnown = true;
            }
          break;
          case Select_Device_SN : // Manufacturer Specific Object
            if (SubIndex == 0x00)
            {
              if (!(Data_32 == fSerial_Number))
              fDevice_select = false;
              RegKnown        = true;
            }
          break;
          case Set_New_Node_ID : // Manufacturer Specific Object
            if (SubIndex == 0x00)
            {
              WriteEEPROM(EEADR_CanOpen_Node_ID,(aData[4]) & 0x7F, 1);
              RegKnown = true;
              Kaltstart_request = true;
            }
          break;
          case Set_SERVO_0_Direct : // Manufacturer Specific Object
            if (SubIndex < 9) // Kein Config register schreiben
            { // SPI Variante
               SPI_CANopen_Send16((uint32_t)SERVO_Registers[SubIndex], Data_16);
              RegKnown = true;
            }
          break;
          case Set_SERVO_1_Direct : // Manufacturer Specific Object
            if (SubIndex < 9) // Kein Config register!
            { // SPI Variante
              SPI_CANopen_Send16((uint32_t)SERVO_Registers[SubIndex] + 0x80, Data_16);
              RegKnown = true;
            }
          break;
          case Set_SERVO_0_EEPROM : // Manufacturer Specific Object: A-Achse
            if (SubIndex < 10)
            {
              WriteEEPROM((uint32_t) (&(SERVO_0 -> Data[SubIndex])), Data_16, 2);
              RegKnown = true;
            }
          break;
          case Set_SERVO_1_EEPROM : // Manufacturer Specific Object: A-Achse
            if (SubIndex < 10)
            {
              WriteEEPROM((uint32_t) (&(SERVO_1 -> Data[SubIndex])), Data_16, 2);
              RegKnown = true;
            }
          break;
          case Set_SERVO_2_EEPROM : // Manufacturer Specific Object: A-Achse
            if (SubIndex < 10)
            {
              WriteEEPROM((uint32_t) (&(SERVO_2 -> Data[SubIndex])), Data_16, 2);
              RegKnown = true;
            }
          break;
          case Set_SERVO_3_EEPROM : // Manufacturer Specific Object: A-Achse
            if (SubIndex < 10)
            {
              WriteEEPROM((uint32_t) (&(SERVO_3 -> Data[SubIndex])), Data_16, 2);
              RegKnown = true;
            }
          break;
          case Set_ManReg_EEPROM : // Manufacturer Specific Object
            if (SubIndex < EManReg_Area_size)
            {
               WriteEEPROM((uint32_t) (&(EManReg -> Data[SubIndex])), Data_16, 2);
              RegKnown = true;
            }
          break;
          case Set_HC12_Register_Byte :
            if ((Data_16 >= HC12_EEPROM_BASE) && ((Data_16 < (HC12_EEPROM_BASE + EEPROM_SIZE))))
              WriteEEPROM(Data_16 - HC12_EEPROM_BASE  ,0xFF & (Data_32 >> 16), 1);
            else
              *(uint8_t *) (Data_32) = 0xFF & (Data_32 >> 16);
            RegKnown = true;
          break;
          case FlashOperation :
            switch (SubIndex)
            {
              case FlashPageCommand :
                if (Data_16 == 0x79)
                {
                  fState_Control |= SC_Bootloader_Enabled;
                  RegKnown = true;
                }
              break;
            }
          break;
          default:

            RegKnown = ((CUSTOM_Register_Write != NULL) && CUSTOM_Register_Write(Register, SubIndex , &Data_32, &Size));
            if (RegKnown)
               OnCUSTOM_Register_Write_DONE();
          break;
        }
      }
      if (StartSegmented)
      {
        vCurrent_SDO_Register = Register;
        // HC12 Releikt
        if (((uint16_t)Register == HC12_EEPROM_BASE))
        {
          StandardAnswer     = false;
        }
        else if (((uint16_t)Register == 0x8000))
        {
          Send_Segmented_Download_Responce(TxIRQData, 0, Size);
          // CAN_EnterCritical(&IER_Store);
          //Send_trans_IRQ1(TxIRQData, INITIATE_DOWNLOAD_RESPONCE, Data_32);
          fState_Control |= SC_Flash_Enabled;
          StandardAnswer     = false;
        }
        else if (((CUSTOM_Register_Write != NULL) && CUSTOM_Register_Write(Register, SubIndex , &Data_32, &Size)))
        {
          Send_Segmented_Download_Responce(TxIRQData, Data_32, Size);
          StandardAnswer     = false;
        }

      }
      if (StandardAnswer)
      {
        if (RegKnown)
          Send_trans_IRQ1(TxIRQData, INITIATE_DOWNLOAD_RESPONCE, 0x00);
        else
          Send_Register_unknown( TxIRQData);
      }
    }
    break;      /* initiate download request */
    case INITIATE_UPLOAD_REQUEST : /* initiate upload request */
    {
      RegKnown = true;
      switch ((uint16_t)Register)  // signed ist hier wichtig sonst funzt 8000 nicht mehr
      {
        case 0x1003 : // manufacturer status register UNSIGNED32 ro O
          Data_32 = Get_Error_Betriebsstunden(SubIndex);
          if (SubIndex < 11)
            Send_trans_IRQ1(TxIRQData, INITIATE_UPLOAD_RESPONCE | SDO_xCS_SIZE_4BYTE, Data_32); // (Errors[aData[3]] + ((uint32_t) Error_Betriebsstunden[aData[3]] << 16))    );
          else
            RegKnown = false;
        break;
        case 0x100A : // manufacturer software version Vis-String const O
          switch (SubIndex)
          {
            case 0  : Send_trans_IRQ1(TxIRQData, INITIATE_UPLOAD_RESPONCE | SDO_xCS_SIZE_4BYTE, * (uint32_t *) fSW_VER); break;
            case 1  : Temp = 0x3030+ (SPI_CANopen_Receive16(0xFF&(int8_t)0x023E));
                      Temp = (Temp >> 8) + (Temp << 8); // Change Order
                      Send_trans_IRQ1(TxIRQData, INITIATE_UPLOAD_RESPONCE | SDO_xCS_SIZE_4BYTE,Temp);
                      break;      // FPGA Software
            case 2  : Send_trans_IRQ1(TxIRQData, INITIATE_UPLOAD_RESPONCE | SDO_xCS_SIZE_4BYTE, * (uint32_t *) UNICAN_VERSION ); break;
            case 3  : Temp = 0x3030+fBL_VER;
                      Temp = (Temp >> 8) + (Temp << 8); // Change Order
                      Send_trans_IRQ1(TxIRQData, INITIATE_UPLOAD_RESPONCE | SDO_xCS_SIZE_4BYTE, Temp ); break;
            default : RegKnown = false;
          }
        break;
        case Set_ManReg_EEPROM : // Manufacturer Specific Object
           if (SubIndex < EManReg_Area_size)
            Send_trans_IRQ1(TxIRQData, INITIATE_UPLOAD_RESPONCE | SDO_xCS_SIZE_4BYTE,  ReadEEPROM((uint32_t)(&(EManReg -> Data[SubIndex])), 2));
          else
            RegKnown = false;
        break;
        case Set_SERVO_0_EEPROM : // Manufacturer Specific Object: X-Achse
          if (SubIndex < 10)
            Send_trans_IRQ1(TxIRQData, INITIATE_UPLOAD_RESPONCE | SDO_xCS_SIZE_4BYTE,  ReadEEPROM((uint32_t) &SERVO_0 -> Data[SubIndex],2));
          else
            RegKnown = false;
        break;
        case Set_SERVO_1_EEPROM : // Manufacturer Specific Object: Y-Achse
          if (SubIndex < 10)
            Send_trans_IRQ1(TxIRQData, INITIATE_UPLOAD_RESPONCE | SDO_xCS_SIZE_4BYTE,  ReadEEPROM((uint32_t) &SERVO_1 -> Data[SubIndex],2));
          else
            RegKnown = false;
        break;
        case Set_SERVO_2_EEPROM : // Manufacturer Specific Object: R-Achse
          if (SubIndex < 10)
            Send_trans_IRQ1(TxIRQData, INITIATE_UPLOAD_RESPONCE | SDO_xCS_SIZE_4BYTE,  ReadEEPROM((uint32_t) &SERVO_2 -> Data[SubIndex],2));
          else
            RegKnown = false;
        break;
        case Set_SERVO_3_EEPROM : // Manufacturer Specific Object: A-Achse
          if (SubIndex < 10)
            Send_trans_IRQ1(TxIRQData, INITIATE_UPLOAD_RESPONCE | SDO_xCS_SIZE_4BYTE,  ReadEEPROM((uint32_t) &SERVO_3 -> Data[SubIndex],2));
          else
            RegKnown = false;
        break;
        case Get_Betriebsstunden : // Manufacturer Specific Object:
          if (SubIndex < 10)
            Send_trans_IRQ1(TxIRQData, INITIATE_UPLOAD_RESPONCE | SDO_xCS_SIZE_4BYTE,  ReadEEPROM(EEADR_Betriebsstunden, 2) );
          else
            RegKnown = false;
        break;
        case Get_Birtday : // Manufacturer Specific Object:
         if (SubIndex < 10)
            Send_trans_IRQ1(TxIRQData, INITIATE_UPLOAD_RESPONCE | SDO_xCS_SIZE_4BYTE,  ReadEEPROM(EEADR_Birthday,2));
          else
            RegKnown = false;
        break;
        case Set_SERVO_0_Direct : // Manufacturer Specific Object: A-Achse
           if (SubIndex < 10)  // SPI Variante
            Send_trans_IRQ1(TxIRQData, INITIATE_UPLOAD_RESPONCE | SDO_xCS_SIZE_4BYTE, SPI_CANopen_Receive16((uint32_t)SERVO_Registers[SubIndex]));
          else
            RegKnown = false;
        break;

        case Set_SERVO_1_Direct : // Manufacturer Specific Object: X-Achse
          if (SubIndex < 10)
            Send_trans_IRQ1(TxIRQData, INITIATE_UPLOAD_RESPONCE | SDO_xCS_SIZE_4BYTE, SPI_CANopen_Receive16((uint32_t)SERVO_Registers[SubIndex] + 0x80));
          else
            RegKnown = false;
        break;
        /* ***************************************************************
         * $180 ist au�erhalb des SPI FPGA Speicherbereichsdes ersten FPGA
         * ***************************************************************
         * case Set_SERVO_2_Direct : // Manufacturer Specific Object: Y-Achse
          if (SubIndex < 10)
            Send_trans_IRQ1(TxIRQData, INITIATE_UPLOAD_RESPONCE | SDO_xCS_SIZE_4BYTE, SPI_CANopen_Receive16((uint32_t)SERVO_Registers[SubIndex] + 0x100));
          else
            RegKnown = false;
        break;
        case Set_SERVO_3_Direct : // Manufacturer Specific Object: R-Achse
          if (SubIndex < 10)
            Send_trans_IRQ1(TxIRQData, INITIATE_UPLOAD_RESPONCE | SDO_xCS_SIZE_4BYTE, SPI_CANopen_Receive16((uint32_t)SERVO_Registers[SubIndex] + 0x180));
          else
            RegKnown = false;
        break;*/
        case Get_CYCLE_State : // Manufacturer Specific Object:
          switch (SubIndex)
          {
            case 0 : Send_trans_IRQ1(TxIRQData, INITIATE_UPLOAD_RESPONCE | SDO_xCS_SIZE_1BYTE, 2); break;
            case 1 : Send_trans_IRQ1(TxIRQData, INITIATE_UPLOAD_RESPONCE | SDO_xCS_SIZE_4BYTE, fCycleMin); break;
            case 2 : Send_trans_IRQ1(TxIRQData, INITIATE_UPLOAD_RESPONCE | SDO_xCS_SIZE_4BYTE, fCycleMax); break;
            default : RegKnown = false;
          }
          break;
        case Get_FIFO_State : // Manufacturer Specific Object:
          switch (SubIndex)
          {
            case 0 : scs = INITIATE_UPLOAD_RESPONCE | SDO_xCS_SIZE_1BYTE; Data_32 = 6;   break;
            case 1 : scs = INITIATE_UPLOAD_RESPONCE | SDO_xCS_SIZE_1BYTE; Data_32 = 0; /*RXC_FIFO.FIFO_Filling*/  break;
            case 2 : scs = INITIATE_UPLOAD_RESPONCE | SDO_xCS_SIZE_1BYTE; Data_32 = 0; /*RXC_FIFO.FIFO_Max_Filling*/  break;
            case 3 : scs = INITIATE_UPLOAD_RESPONCE | SDO_xCS_SIZE_1BYTE; Data_32 = TXC_FIFO.FIFO_Filling;   break;
            case 4 : scs = INITIATE_UPLOAD_RESPONCE | SDO_xCS_SIZE_1BYTE; Data_32 = TXC_FIFO.FIFO_Max_Filling;   break;
            case 5 : scs = INITIATE_UPLOAD_RESPONCE | SDO_xCS_SIZE_2BYTE; Data_32 = fRX_Errors;   break;
            case 6 : scs = INITIATE_UPLOAD_RESPONCE | SDO_xCS_SIZE_2BYTE; Data_32 = fTX_Errors;   break;
            default : RegKnown = false;
          }
          if (RegKnown)
            Send_trans_IRQ1(TxIRQData, scs, Data_32);
        break;
        case 0x607D : // ARRAY software_position_limit Integer32 rw O
          i = SubIndex - 1;
          if ((i & 0x07) == i)
             Send_trans_IRQ1(TxIRQData, INITIATE_UPLOAD_RESPONCE | SDO_xCS_SIZE_4BYTE, fSoftware_position_limit[i]);
          else
            RegKnown = false;
        break;
        default:
          // HC12 Releikt
          if (((uint16_t)Register == 0x0000)  || ((uint16_t)Register == 0x0800) || ((uint16_t)Register == HC12_EEPROM_BASE) || ((uint16_t)Register == 0x8000))
          {
            if ((uint16_t)Register == 0x0000)
            {
              Data_32 = ((uint32_t) APB1PERIPH_BASE); // Adresse
              Size             = 0x200;
            }

            if ((uint16_t)Register == 0x0800)
            {
              #ifdef SRAM_BASE
                Data_32 = ((uint32_t) SRAM_BASE); // Adresse  //fr�her SRAM1_BASE
              #else
                Data_32 = ((uint32_t) SRAM1_BASE); // Adresse  //fr�her SRAM1_BASE
              #endif
              Size             = 0x3FF;
            }
            if ((uint16_t)Register == HC12_EEPROM_BASE)
            {
              if ((pCANEEPROM != NULL))
              {
                Data_32 = ((uint32_t)  pCANEEPROM->GetMirrorAdr()); // Adresse
                Size             = 0x300;
              }
              else
                RegKnown = false;
            }
            if ((uint16_t)Register == 0x8000)
            {
              Data_32 = ((uint32_t) FLASH_BASE);  // Gr��e des Flashspeichers
              #if (!RAM_BOOTLOADER)
              Size             =  BOOTLOADER_BASE - FLASH_BASE;
              #else
                #if defined(STM32F10X_MD) || defined(STM32F103xB)
                Size             =  FLASH_BANK1_END  - FLASH_BASE;
                #else
                Size             =  FLASH_END  - FLASH_BASE;
                #endif
              #endif
            }
            if (RegKnown)
              Send_Segmented_Upload_Responce(TxIRQData, Data_32 , Size);
          }

          else if (Canopen_Register_Read((uint32_t*)&ConstantenRegisters, sizeof(ConstantenRegisters), Register, SubIndex, &Data_32, &Size))
            Send_trans_IRQ1(TxIRQData, INITIATE_UPLOAD_RESPONCE |  SDO_xCS_SIZE_4BYTE | (((4-Size) & 3) << 2) , Data_32);
          else if (Canopen_Register_Read((uint32_t*)&VariablenRegisters, sizeof(VariablenRegisters), Register, SubIndex, &Data_32, &Size))
            Send_trans_IRQ1(TxIRQData, INITIATE_UPLOAD_RESPONCE |  SDO_xCS_SIZE_4BYTE | (((4-Size) & 3) << 2) , Data_32);
          else if (CUSTOM_Register_Read(Register, SubIndex, &Data_32, &Size))
          {
            if (Size > 4)
              Send_Segmented_Upload_Responce( TxIRQData, Data_32 , Size);
            else
            {
              Send_trans_IRQ1(TxIRQData, INITIATE_UPLOAD_RESPONCE |  SDO_xCS_SIZE_4BYTE | (((4-Size) & 3) << 2) , Data_32);
              OnCUSTOM_Register_Read_DONE();
            }
          }
          else
            RegKnown = false;
        break;
      }
      if (!RegKnown)
        Send_Register_unknown( TxIRQData);
    }
    break; /* initiate upload request */
    case DOWNLOAD_SEGMENT_REQUEST : /* segmented download request    EEPROM brennen */
      if (fState_Control & SC_Segmented_Download)
      {
        Size = 7 - ((ccs >> 1) & 0x7);

        if (Size == 0) // Nicht Spezifiziert
          Size = 7;
        for (i=1;((i <= Size) && (i <= (fBytes_to_Send)));i++)
        {
          if ((vCurrent_SDO_Register == HC12_EEPROM_BASE) && (((uint32_t)fPointer_for_Send) < (uint32_t) EEPROM_SIZE))
            WriteEEPROM((uint32_t) fPointer_for_Send ,aData[8-i],1);
          else
            *fPointer_for_Send = aData[i];
          fPointer_for_Send = fPointer_for_Send + 1;
        }

        aData[1] = aData[2] = aData[3] = 0;
        Send_trans_IRQ1( TxIRQData, DOWNLOAD_SEGMENT_RESPONCE | (fState_Control & SC_Toggle_Bit), 0); // e = 0, 32kB

        if (fState_Control & SC_Toggle_Bit) fState_Control &= ~SC_Toggle_Bit;
        else                               fState_Control |=  SC_Toggle_Bit; // = SEGMENTED_TOGGLE_BIT

        if ((fBytes_to_Send < 7) || ((ccs & SEGMENTED_NO_MORE_SEGMENTS) != 0))
        {
          fState_Control &= ~SC_Segmented_Download;
          OnCUSTOM_Register_Write_DONE();
        }
        fBytes_to_Send    -= 7;
      }
      else
        Send_Register_unknown(TxIRQData);
    break;
    case UPLOAD_SEGMENT_REQUEST : /* segmented upload request */
      if ((ccs ^ fState_Control) & SC_Toggle_Bit)
        Send_Register_unknown(TxIRQData);
      if (fState_Control & SC_Segmented_Upload )
      {
        if (fBytes_to_Send < 8)
        {
          Send_trans_IRQ2(UPLOAD_SEGMENT_RESPONCE | SEGMENTED_NO_MORE_SEGMENTS | (fState_Control & SC_Toggle_Bit) | ((7-fBytes_to_Send) << 1), fPointer_for_Send); // e = 0, 32kB
          fState_Control &= ~SC_Segmented_Upload;
          OnCUSTOM_Register_Read_DONE();
        }
        else
        {
          Send_trans_IRQ2(UPLOAD_SEGMENT_RESPONCE | (fState_Control & SC_Toggle_Bit) | SEGMENTED_7BYTES , fPointer_for_Send); // e = 0, 32kB
          fBytes_to_Send    -= 7;
          fPointer_for_Send += 7;
        }
        if (fState_Control & SC_Toggle_Bit) fState_Control &= ~SC_Toggle_Bit;
        else                                fState_Control |=  SC_Toggle_Bit; // = SEGMENTED_TOGGLE_BIT
      }
      else
        Send_Register_unknown(TxIRQData);
    break;
    case ABORT_DOMAIN_TRANSFER : /* Abort SDO Transfer Protocol */
          fState_Control &=  ~SC_Segmented_Upload;
    break;
    default:
      Send_Register_unknown(TxIRQData);
    break;
  }
}


/*----------------------------------------------------------------------------
  CAN Receiver receive
 *----------------------------------------------------------------------------*/
void tUnican::CAN_Receiver(CAN_RxHeaderTypeDef *pHeader, uint8_t aData[])
{
  uint8_t number;

  if (fDevice_select)
  {
    if ((pHeader->StdId & FUNCTION_MASK) == FUNCTION_CODE_NMT)
      NMT_Receive(pHeader, aData);
    else if ((pHeader->StdId & FUNCTION_MASK) == FUNCTION_CODE_SYNC)
      SYNC_Receive(pHeader, aData);
    else if ((pHeader->StdId & FUNCTION_MASK) == FUNCTION_CODE_TIME)
      TIME_Receive(pHeader, aData);
    else if (((pHeader->StdId & ID_MASK) == fNode_ID) && (pHeader->StdId >= FUNCTION_CODE_PDO_TX_BASE))
    {  /* Peer_to_Peer */
      number = (pHeader->StdId - FUNCTION_CODE_PDO_TX_BASE) >> 8;
      if (number < 4)
      {
        if ((pHeader->StdId & FUNCTION_CODE_SYNC) == 0)
          RXPDO_Receive(number, pHeader, aData);
        else
          TXPDO_Receive(number, pHeader, aData);    // RTR Objekte
      }
      else
      {
        if ((pHeader->StdId & FUNCTION_MASK) == FUNCTION_CODE_SDO_RX)
          RXSDO_Receive(pHeader, aData);
        else if ((pHeader->StdId & FUNCTION_MASK) == FUNCTION_CODE_LIVEGUARD)
          LIVEGUARD_Receive(pHeader, aData);
        else if (pHeader->StdId == FUNCTION_CODE_LSS)
          LSS_Receive(pHeader, aData);
      }
    }
  }
  else
  {
    if ((pHeader->StdId & FUNCTION_MASK) == FUNCTION_CODE_NMT)
      fDevice_select = true;
   if ((pHeader->StdId & FUNCTION_MASK) == FUNCTION_CODE_SDO_RX)
      fDevice_select = true;
  }

}

void tUnican::StopServices(void)
{
  uint8_t EEPROM_DONE  = 0;

  while(!(CAN_FIFO_EMPTY(&TXC_FIFO) && CAN_TRANSMITTER_EMPTY() &&  EEPROM_DONE))
  {
    if (pCANEEPROM != NULL)
    {
      pCANEEPROM->EEPROM_WRITER();
      EEPROM_DONE = pCANEEPROM->EEPROM_WRITER_DONE();
    }
    else
      EEPROM_DONE = 1;
     CAN_TRANSMITTER();
  }

  //CAN_EnterCritical(&IER_Store);
  //while (!CAN_checkMailboxEmpty (fMyCanCtrl, Standard_Mailbox, &LastError))
  while (!CAN_TRANSMITTER_EMPTY())
    CAN_TRANSMITTER();
}

/*----------------------------------------------------------------------------
  public declarations
 *----------------------------------------------------------------------------*/
tUnican::tUnican(CAN_HandleTypeDef *hcan, tServoEEPROM *EEPROM)
{
  fMyInstanceIndex = fMaster_InstanceCounter;
  Master_handlers[fMaster_InstanceCounter++] = this; // register this instance against the id

  //static CanTxMsgTypeDef TxMessage;
  //static CanRxMsgTypeDef RxMessage;
  fEnabled            = false;
  fNode_ID            = 0;
  fBL_VER             = 0;
  fSerial_Number      = 0;
  fLivetimer          = 0;
  fLivetimer_sekunden = 0;
  fBirthday           = 0;
  fBetriebsstunden    = 0;
  fhcan = hcan;
  pCANEEPROM = EEPROM;


  Reset_FIFO(&TXC_FIFO);
  Reset_Data();

 //SysTick->VAL;

}

tUnican::~tUnican(void)
{
  Reset_Data();
  fMaster_InstanceCounter--;
  for (int i=fMyInstanceIndex; i<fMaster_InstanceCounter; i++)
  {
    Master_handlers[i] = Master_handlers[i+1];
    Master_handlers[i]->fMyInstanceIndex--;
  }
  // fifo muss auf den HEAP und hier wieder gekillt werden;
}


/*----------------------------------------------------------------------------
  Disable / Enable CAN Interrupts
 *----------------------------------------------------------------------------*/
//macht die main....

uint8_t tUnican::CheckIRQ(void)
{
  return (fhcan -> Instance->IER & (CAN_IER_FMPIE0 | CAN_IER_FMPIE1)) != 0;
}


uint32_t tUnican::ReadEEPROM(uint16_t Addr, uint8_t count)
{
  if (pCANEEPROM != NULL)
    return pCANEEPROM->EEReadValue(Addr, count);
  else
    return 0;
}

void tUnican::WriteEEPROM(uint16_t Addr, uint32_t Value, uint8_t count)
{
  if (pCANEEPROM != NULL)
    pCANEEPROM->EEStoreValue(Addr, Value, count);
}

uint8_t tUnican::OpenCan(uint8_t Def_Node_ID, char const* c_NAME, char const* c_HW_VER, char const* c_SW_VER)
{
  uint8_t     vERROR_Code;
  fBL_VER        = BootloaderVersion();
  fHW_NAME       = c_NAME;
  fHW_VER        = c_HW_VER;
  fSW_VER        = c_SW_VER;
  fSerial_Number = 0;
  vERROR_Code    = 0;

  HAL_CAN_Stop(fhcan);
  Reset_Data();

  if (vERROR_Code == 0)  //solange keine Baudrate gesetzt wird egal...
  {

    fNode_ID         =  ReadEEPROM(EEADR_CanOpen_Node_ID,1);
    fSerial_Number   =  ReadEEPROM(EEADR_Serial_Number, 4);
    fBirthday        =  ReadEEPROM(EEADR_Birthday, 2);
    fBetriebsstunden =  ReadEEPROM(EEADR_Betriebsstunden, 2);

    if (( ReadEEPROM(EEADR_EEData_OK,1) != 0xAD) || (fNode_ID == 0x00) || (fNode_ID & 0x80))
    {
      fNode_ID = Def_Node_ID;
      WriteEEPROM(EEADR_EEData_OK,0xAD,1);
      WriteEEPROM(EEADR_CanOpen_Node_ID,fNode_ID,1);
      if (fSerial_Number == (uint32_t)0xFFFFFFFF)
        WriteEEPROM(EEADR_Serial_Number, 0x07000000, 4);
      if (fBetriebsstunden == 0xFFFF)
        WriteEEPROM(EEADR_Betriebsstunden, 0x0000 , 2);
      if ( ReadEEPROM(EEADR_Errors,2) == 0xFFFF)
        WriteEEPROM(EEADR_Errors, 0x0000 , 2);
    }


    if (fhcan->State != HAL_CAN_STATE_LISTENING) // Not Already LISTENING
    {
      FilterMask(fhcan, 0, RXFIFO, FUNCTION_CODE_NMT, 0);
      FilterMask(fhcan, 1, RXFIFO, FUNCTION_CODE_SYNC, 0);
      FilterMask(fhcan, 2, RXFIFO, FUNCTION_CODE_TIME, 0);
      FilterMask(fhcan, 3, RXFIFO, FUNCTION_CODE_LSS, 0);
      FilterMask(fhcan, 4, RXFIFO, fNode_ID, 0x780);

      if (HAL_CAN_Start(fhcan) != HAL_OK)
      {
         /* Start Error */
         Error_Handler();
      }

      if (HAL_CAN_ActivateNotification(fhcan, CAN_IT_RX_FIFO0_MSG_PENDING) != HAL_OK)
      {
         /* Notification Error */
         Error_Handler();
      }
    }

    Reset_Communication  = false;


    //fNode_ID = Def_Node_ID;//0x051;//Def_Node_ID;
    TxHeader.StdId = fNode_ID;
    TxHeader.ExtId = 0x01;
    TxHeader.RTR = CAN_RTR_DATA;
    TxHeader.IDE = CAN_ID_STD;
    TxHeader.DLC = 2;

    fHeartbeatTimer             = 10;
    while(fHeartbeatTimer);       // �berm��igen Aktionismus bremsen. HAL_TIM_Base_Start_IT(&htim2); nicht vergessen!


       // CRIER |= 0x01;             // Enable Recive Interrupt
    Send_NODEGUARD(0,0);      // Bootup Protokoll
  }

    //Serial_Number = Get_EEPROM((uint32_t) EEADR_Serial_Number + 0, 4);
    //if (!CAN_start (fMyCanCtrl))
    //  vERROR_Code = 0x02;
  /*ToDo: brauch ich das Timer setzen?
  RCC_TIMPeriphClock_Cmd(STATISTIK_TIMER, ENABLE);

  TIM_TimeBase_InitStructure.TIM_ClockDivision = TIM_CKD_DIV1;
  TIM_TimeBase_InitStructure.TIM_CounterMode = TIM_CounterMode_Up;
  TIM_TimeBase_InitStructure.TIM_Period   = 32767;
  TIM_TimeBase_InitStructure.TIM_Prescaler = 21;    // 4 MHz
  TIM_TimeBaseInit(STATISTIK_TIMER, &TIM_TimeBase_InitStructure);
  TIM_Cmd(STATISTIK_TIMER, ENABLE);*/

  // Test29BIT();
  fEnabled = true;
  return (vERROR_Code == 0);
}

uint8_t tUnican::FilterMask(CAN_HandleTypeDef *hcan, uint8_t Number, uint8_t FIFO, uint32_t id, uint32_t mask)
{
  // Es gibt einen Identifier_List_mode und einen Identifier_MAsk_mode
  // Diese Funktion verwendet nur den Maskenmodus
  uint32_t      Acceptance_Mask =~mask;
   CAN_FilterTypeDef  sFilterConfig;

   if (fhcan->Instance == CAN1)
      sFilterConfig.FilterBank = 0 + Number;
   else
      sFilterConfig.FilterBank = 14 + Number;


   sFilterConfig.FilterMode = CAN_FILTERMODE_IDMASK;
   sFilterConfig.FilterScale = CAN_FILTERSCALE_32BIT;

   sFilterConfig.FilterIdHigh = (id << 5);
   sFilterConfig.FilterIdLow = 0x0000;
   sFilterConfig.FilterMaskIdHigh = (Acceptance_Mask << 5) ;
   sFilterConfig.FilterMaskIdLow  = 0x0004;  // Extended ID nicht durchlassen
   sFilterConfig.FilterFIFOAssignment = FIFO;
   sFilterConfig.FilterActivation = ENABLE;
   sFilterConfig.SlaveStartFilterBank = 14;

   if (HAL_CAN_ConfigFilter(fhcan, &sFilterConfig) != HAL_OK)
   {
     /* Filter configuration Error */
     Error_Handler();
     return 0;
   }
   else return 1;


}

void tUnican::Call_CIA_TIMERS (void)
{
  if (fHeartbeatTimer) fHeartbeatTimer--;
  if (fHeartbeatTime && !fHeartbeatTimer)
  {
    if (fState_Control & SC_NMT_STOPPED)  Send_NODEGUARD(0, 4);   // Stopped
    else if (fState_Control & SC_Preoperational)  Send_NODEGUARD(0, 127); // Pre OPERATIONAL
    else                                          Send_NODEGUARD(0, 5);   // OPERATIONAL
    fHeartbeatTimer = fHeartbeatTime; // Heartbeat Protokoll
  }

  fLivetimer ++;
  if (fLivetimer > 999)
  {
    fLivetimer_sekunden++;
    fLivetimer = 0;
  }
  if (pCANEEPROM != NULL)
    pCANEEPROM->EEPROM_TIMER_CALL_EVERY_MS();
}

uint8_t tUnican::Handle_Coldstart()
{
  uint8_t wait_for_EEPROM = 0;

  #if (!defined(STM32F767xx))
   if (fState_Control & SC_Flash_Enabled)
   {
     StopServices();
     #if (RAM_BOOTLOADER)
     FlashToolLoader();
     #else
     CiaBootLoader(1);
     #endif

     /*
     while (1)
     {
       while (CAN2->RF0R & CAN_RF0R_FMP0)
       {
         CAN_rdMsg (fMyCanCtrl, RXFIFO, &CAN_RxMsg[fMyCanCtrl-1]);
         Receiver(fMyCanCtrl);
       }
       CAN2->RF0R |= CAN_RF0R_FOVR0;
       CAN2->RF0R |= CAN_RF0R_FULL0;
     }
     */
   }
  #endif

  #if defined(STM32F10X_MD) || defined(STM32F103xB)
  #else
  if (fState_Control & SC_Bootloader_Enabled)
  {
    while (!CAN_TRANSMITTER_EMPTY())
      CAN_TRANSMITTER();

    __disable_irq();
    BootLoaderInit(1);
  }
  #endif

  if (pCANEEPROM != NULL)
    wait_for_EEPROM = !pCANEEPROM->EEPROM_WRITER_DONE();
  else
    wait_for_EEPROM = 0;


  if (Kaltstart_request && CAN_TRANSMITTER_EMPTY() && !wait_for_EEPROM)
  {
    Reset_Communication = true;
    return 1;
  }
  return 0;
}


tSERVO_EEPROM_Default tUnican::GetEEPROMRegleparameters(uint8_t Achse)
{
  uint8_t i;
  uint16_t vData;

  tSERVO_EEPROM_Default result;
  for (i=0;i<(sizeof(result)/sizeof(uint16_t));i++)
  {
    vData =  ReadEEPROM(EEADR_SERVO_0_Parameter_B + (Achse * 20) + 2*i,2);
    *( ((uint16_t*)(&result)) + i) = vData;
  }
  return result;
}

void tUnican::SetEEPROMRegleparameters(uint8_t Achse, tSERVO_EEPROM_Default Data)
{
  uint8_t i;
  uint16_t vData;

  for (i=0;i<(sizeof(Data)/sizeof(uint16_t));i++)
  {
    vData = *( ((uint16_t*)(&Data)) + i);
    WriteEEPROM(EEADR_SERVO_0_Parameter_B + (Achse * 20) + 2*i, vData , 2);
  }
}

uint8_t tUnican::Handle_TPDO(uint8_t TPDO_Number, uint8_t sync, uint8_t size, uint8_t byte0, uint8_t byte1, uint8_t byte2,
                          uint8_t byte3, uint8_t byte4, uint8_t byte5, uint8_t byte6, uint8_t byte7)
{
  uint8_t  i, TPDO_enabled_temp, Result, Send, Index;
  uint8_t Data[8];
  struct tcan_buf tempBuf;
  Result = true;

  if ((TPDO_Number >= 1) && (TPDO_Number <= 4))
  {
    Index             = TPDO_Number - 1;             // Index Initialisieren
    TPDO_enabled_temp = fTPDO_enabled[Index];         // Zustand einfrieren
    if (TPDO_enabled_temp)
    {
	  Send     = 0;
	  Data[0] = byte0;
	  Data[1] = byte1;
	  Data[2] = byte2;
	  Data[3] = byte3;
	  Data[4] = byte4;
	  Data[5] = byte5;
	  Data[6] = byte6;
	  Data[7] = byte7;

	  if (sync == 1)                                // sync  PDO
	  {
	    if (fSend_sync_PDO[Index])
	    {
		  Send                   = true;
		  fSend_sync_PDO[Index] = false;
	    }
	  }
	  else                                         // event PDO
	  {
	    Send = !fTPDO_enabled_old[Index];
	    for (i = 0; i < 8; i++)
	    {
	      if (fTPDO_DATA_OLD[TPDO_Number - 1][i] != Data[i])
        {
          Send = true;
          break;
        }
	    }
	  }

	  if (Send)
      {


        tempBuf.id_field = (0x0080 | fNode_ID) + (TPDO_Number << 8);
        tempBuf.dlc = size;
        for (i = 0; i < 8; i++)
          tempBuf.data_field[i] = fTPDO_DATA_OLD[TPDO_Number - 1][i] =  Data[i];

        if (CAN_FIFO_ADD(&TXC_FIFO, &tempBuf, 0))
        {
          fTPDO_enabled_old[Index] = TPDO_enabled_temp;
        }
        else
          Result = false;    // Kanal war belegt
      }
    }
  }
  return (Result);
}

void tUnican::Handle_Emergency(void)
{
  uint8_t i;
  struct tcan_buf tempBuf;
  // LEC[6:4]
  //000: No Error
  //001: Stuff Error
  //010: Form Error
  //011: Acknowledgment Error
  //100: Bit recessive Error
  //101: Bit dominant Error
  //110: CRC Error
  //111: Set by software

  if (BUSOFF())
     fEmergency_Error_code = Error_Passive_Mode;
  if (!CheckIRQ())
     fEmergency_Error_code = 0x0815;

  CAN_TRANSMITTER();
  Runtime_Statistik();


	if ((fEmergency_Error_code != fEmergency_Error_code_old) ||
		(fError_register       != fError_register_old))
	{
	  tempBuf.id_field = fNode_ID + FUNCTION_CODE_EMERGENCY;
	  tempBuf.dlc = 8;
	  for (i = 0; i < 8; i++)
		  tempBuf.data_field[i] = 0;
	  tempBuf.data_field[0]          = fEmergency_Error_code << 8;
	  tempBuf.data_field[1]          = fEmergency_Error_code;
	  tempBuf.data_field[2]          = fError_register;


	  if (CAN_FIFO_ADD(&TXC_FIFO, &tempBuf, 0))
	  {
		fEmergency_Error_code_old  = fEmergency_Error_code;
		fError_register_old        = fError_register;


		if (fEmergency_Error_code)
		  Set_Error_Betriebsstunden(fEmergency_Error_code);
	  }

  }



}


 // fhcan->Errorcode
 // HAL_CAN_ERROR_NONE      0x00000000U    /*!< No error             */
 // HAL_CAN_ERROR_EWG       0x00000001U    /*!< EWG error            */
 // HAL_CAN_ERROR_EPV       0x00000002U    /*!< EPV error            */
 // HAL_CAN_ERROR_BOF       0x00000004U    /*!< BOF error            */
 // HAL_CAN_ERROR_STF       0x00000008U    /*!< Stuff error          */
 // HAL_CAN_ERROR_FOR       0x00000010U    /*!< Form error           */
 // HAL_CAN_ERROR_ACK       0x00000020U    /*!< Acknowledgment error */
 // HAL_CAN_ERROR_BR        0x00000040U    /*!< Bit recessive        */
 // HAL_CAN_ERROR_BD        0x00000080U    /*!< LEC dominant         */
 // HAL_CAN_ERROR_CRC       0x00000100U    /*!< LEC transfer error   */
 // HAL_CAN_ERROR_FOV0      0x00000200U    /*!< FIFO0 overrun error  */
 // HAL_CAN_ERROR_FOV1      0x00000400U    /*!< FIFO1 overrun error  */
 // HAL_CAN_ERROR_TXFAIL    0x00000800U    /*!< Transmit failure     */
 //

void tUnican::CAN_Error(void)
{
  if (fhcan->ErrorCode & ~512)
    Last_CAN_Error = CAN_Controllerstatus();

}

uint8_t tUnican::BUSOFF(void)
{
  return CAN_Controllerstatus() & CAN_ESR_BOFF;
}


/* Statistikwerte werden in 1/8 �s angegeben */
void tUnican::Runtime_Statistik(void)
{
  float Timer;					       // in ms
  static float vRT_Timer;		     // in ms
  float Delta;					        // in  ms
  uint32_t vCycleTime =0 ;			// in 1/8 �s

  uint32_t Tick = uwTick;   // Hinweis: Dem STM32F103 Treiber fehlt in der stm32f1xx_hal.h das: extern __IO uint32_t uwTick;
  Timer = Tick+(SysTick->LOAD-SysTick->VAL)/(SysTick->LOAD+1.0); //

  Delta = (Timer-vRT_Timer);
  if (Delta < 0.0)
  {
    Timer++;
    Delta++;
  }

  if (fCycleMax > 0)
  {
    vCycleTime = Delta * 8000;
    if (vCycleTime > fCycleMax)
      fCycleMax = vCycleTime;
    if (vCycleTime < fCycleMin)
      fCycleMin = vCycleTime;
  }
  else
    fCycleMax  = 1;

  vRT_Timer  = Timer;

  // Betriebsstunden Z�hler
  if ((fLivetimer_sekunden == 3600) || (fLivetimer_sekunden == 7200))
  {
    fLivetimer_sekunden++;
    fBetriebsstunden++;
  }

  if (fLivetimer_sekunden > 10801)
  {
    fBetriebsstunden++;
    WriteEEPROM(EEADR_Betriebsstunden, fBetriebsstunden , 2);
    fLivetimer_sekunden = 0;
  }

  if (pCANEEPROM != NULL)
    pCANEEPROM->EEPROM_WRITER();
}


uint32_t tUnican::Get_Error_Betriebsstunden(uint8_t n)
{
  // W�chst nach kleineren Zahlen
  uint8_t First_in, Last_in, count, Point;
  uint32_t  Result;
  First_in = Intervall(1, ReadEEPROM(EEADR_Errors , 1) & 0xFF,10);
  Last_in  = Intervall(1, ReadEEPROM(EEADR_Error_Betriebsstunden , 1) & 0xFF ,10);
  if (((ReadEEPROM(EEADR_Errors , 1) & 0xFF) == 0) || (n > (Error_Betriebsstunden_Size - 1))) // Abw�rtskompatibel
    return 0;
  else if (First_in >= Last_in)
  count = 1 + First_in - Last_in;
  else
    count = (Error_Betriebsstunden_Size + First_in) - Last_in;
  if (n == 0)
    return count;
  else if (n <= count)
  {
    Point  = Last_in + n - 1;
    if (Point > (Error_Betriebsstunden_Size - 1))
      Point = 1 + Point - (Error_Betriebsstunden_Size);
    Result =  ReadEEPROM(EEADR_Errors + 2*Point, 2) + (ReadEEPROM(EEADR_Error_Betriebsstunden + 2*Point, 2) << 16);
    return Result;
  }
  else return 0;
}

uint8_t tUnican::Set_Error_Betriebsstunden(uint16_t Error_code)
{
  // W�chst nach kleineren Zahlen
  unsigned char Last_in, First_in;
  if (pCANEEPROM != NULL)
  {
    if ( pCANEEPROM->EEPROM_WRITER_DONE())
    {
      First_in = Intervall(1, ReadEEPROM(EEADR_Errors , 1) & 0xFF,10);
      Last_in  = Intervall(1, ReadEEPROM(EEADR_Error_Betriebsstunden , 1) & 0xFF ,10);
      if ((ReadEEPROM(EEADR_Errors , 1) & 0xFF) != 0)
      {
        Last_in  = Last_in - 1;
        if (Last_in == 0)
          Last_in = (Error_Betriebsstunden_Size - 1);
        if (First_in == Last_in)
          First_in = First_in - 1;
        if (First_in == 0)
          First_in = (Error_Betriebsstunden_Size - 1);
      }
      else
        Last_in = First_in; // Das Erste nach dem L�schen
       WriteEEPROM(EEADR_Errors + 2*Last_in, Error_code, 2);
       WriteEEPROM(EEADR_Error_Betriebsstunden + 2*Last_in, fBetriebsstunden, 2);
       WriteEEPROM(EEADR_Errors, First_in, 1);
       WriteEEPROM(EEADR_Error_Betriebsstunden, Last_in, 1);
      return true;
    }
    else return false;
  }
  else
    return true;
}

int32_t tUnican::Get_software_position_limit(uint8_t n)
{
  if (n<8)
    return fSoftware_position_limit[n];
  else
    return 0;
}

void  tUnican::Set_software_position_limit(uint8_t n, int32_t value)
{
  if (n<8)
    fSoftware_position_limit[n] = value;
}


uint8_t tUnican::Connected(void)
{
  return (fState_Control & SC_Preoperational) != 0;
}


uint32_t tUnican::CAN_Controllerstatus(void)
{
  //CAN_TypeDef *pCAN = (ctrl == 1) ? CAN1 : CAN2;
  uint32_t result;
  uint8_t  RXFIFO_Messages;
  // Zur Info
  RXFIFO_Messages  = fhcan->Instance->RF0R & (uint32_t) 0x03;
  RXFIFO_Messages += fhcan->Instance->RF1R & (uint32_t) 0x03;

  result  = fhcan->Instance->ESR;
  if (fhcan->Instance->RF1R & CAN_RF1R_FOVR1) result |= (uint32_t) 0x8000;   // FIFO1_Overrun
  if (fhcan->Instance->RF1R & CAN_RF1R_FULL1) result |= (uint32_t) 0x4000;   // FIFO1_Warning
  if (fhcan->Instance->RF0R & CAN_RF0R_FOVR0) result |= (uint32_t) 0x2000;   // FIFO1_Overrun
  if (fhcan->Instance->RF0R & CAN_RF0R_FULL0) result |= (uint32_t) 0x1000;   // FIFO1_Warning
  return(result);
}


uint8_t tUnican::CAN_TRANSMITTER(void)
{
  tcan_buf txBuf;
  uint8_t Not_used, i;
  uint8_t Result = true;

  if (fEnabled && CAN_TRANSMITTER_EMPTY())
  {
    if (CAN_FIFO_GET(&TXC_FIFO, &txBuf, &Not_used))
    {

      TxHeader.StdId = txBuf.id_field;
      TxHeader.ExtId = 0x01;
      TxHeader.IDE   = CAN_ID_STD;
      // HC12 RTR = ((*(unsigned short *) &(Receive_temp.id_field)) & (unsigned short) 0x0010);
      TxHeader.RTR   = CAN_RTR_DATA;
      TxHeader.DLC   = txBuf.dlc;
      for (i=0;i<8;i++)
        TxData[i]     = txBuf.data_field[i];

      uint32_t prim;
      prim = __get_PRIMASK();
      __disable_irq();
      Result = HAL_CAN_AddTxMessage(fhcan, &TxHeader, TxData, &TxMailbox) == HAL_OK;
      if (!prim)
      __enable_irq();

    }
  }
  // Sometimes when HAL_CAN_Transmit_IT Locked FHCAN Receiver not restarted from IR
  return (Result);
}

uint8_t tUnican::CAN_TRANSMITTER_EMPTY(void)
{
  return !HAL_CAN_IsTxMessagePending(fhcan, 0x7);
}


uint8_t tUnican::CAN_FIFO_EMPTY(struct tCAN_FIFO *FIFO)
{
  return (FIFO -> FIFO_out_Pointer == FIFO -> FIFO_in_Pointer);
}
uint8_t tUnican::CAN_FIFO_GET(struct tCAN_FIFO *FIFO,struct tcan_buf *Data, uint8_t *CIDACin)
{
  struct tcan_buf *temp;
  uint8_t Pointer_temp;
  uint8_t result = false;
  uint32_t prim;
  prim = __get_PRIMASK();
    __disable_irq();
  if (FIFO -> FIFO_out_Pointer != FIFO -> FIFO_in_Pointer)
  {     
     Pointer_temp = FIFO -> FIFO_out_Pointer;
     temp = &(FIFO -> can_buf[Pointer_temp]);
     *Data = *temp; 
     *CIDACin = Data -> unused[0];
     FIFO -> FIFO_out_Pointer = Pointer_temp + 1; 
     if (FIFO -> FIFO_Filling > FIFO -> FIFO_Max_Filling)
       FIFO -> FIFO_Max_Filling = FIFO -> FIFO_Filling;
     FIFO -> FIFO_Filling     = FIFO -> FIFO_Filling - 1;
     if (FIFO -> FIFO_out_Pointer >= CAN_FIFO_Size)
       FIFO -> FIFO_out_Pointer = 0;
     result = true;
  }
  if (!prim)
    __enable_irq();
  return result;
}

uint8_t tUnican::CAN_FIFO_ADD(struct tCAN_FIFO *FIFO, struct tcan_buf *Data, uint8_t CIDACin)
{
  uint8_t result, Pointer_temp;
  struct tcan_buf *temp;
  result = false;
  uint32_t prim;
  prim = __get_PRIMASK();
  __disable_irq();

  if (FIFO ->FIFO_in_Pointer < CAN_FIFO_Size) // Schutz
  {
    Pointer_temp = FIFO -> FIFO_in_Pointer;
    temp = &(FIFO -> can_buf[Pointer_temp]);  
    FIFO -> FIFO_in_Pointer = Pointer_temp + 1;
    if (FIFO -> FIFO_in_Pointer >= CAN_FIFO_Size)
      FIFO -> FIFO_in_Pointer = 0;
    if (FIFO -> FIFO_in_Pointer == FIFO -> FIFO_out_Pointer)
      FIFO -> FIFO_in_Pointer = Pointer_temp;  // result false;
	  else // Daten schreiben
  	{
  	  *temp = *Data;                               // Bei �berlauf wird das letzte Paket Priori�t verworfen
      temp -> unused[0]     = CIDACin;             // Bei �berlauf wird das letzte Paket Priori�t verworfen
      FIFO -> FIFO_Filling  = FIFO -> FIFO_Filling + 1;  
      result = true;
	  }
  }
  else
    Reset_FIFO(FIFO); // result false;
  if (!prim)
    __enable_irq();
  return result;
}

void tUnican::Reset_FIFO(struct tCAN_FIFO *FIFO)
{
  uint32_t prim;
  prim = __get_PRIMASK();
    __disable_irq();
  FIFO -> FIFO_in_Pointer   = 0;
  FIFO -> FIFO_out_Pointer  = 0;
  FIFO -> FIFO_Filling      = 0;
  FIFO -> FIFO_Max_Filling  = 0;
  if (!prim)
    __enable_irq();
}

void tUnican::CAN_EnterCritical(void)
{
  uint32_t prim;
  prim = __get_PRIMASK();
  __disable_irq();
  CAN_IER_BUFFER = fhcan->Instance->IER;
  fhcan->Instance->IER = 0;
  if (!prim)
    __enable_irq();

  /*
  // Disable
  MOV R0, #1
  MSR PRIMASK, R0
  MOV R0, #1
  MSR FAULTMASK, R0

  // Enable
  MOV R0, #0
  MSR PRIMASK, R0
  MOV R0, #0
  MSR FAULTMASK, R0
  */
}

void tUnican::CAN_LeaveCritical(void)
{
  fhcan->Instance->IER = CAN_IER_BUFFER;
}
